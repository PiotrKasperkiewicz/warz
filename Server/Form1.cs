/*
 * Copyright (c) 2013 Tomasz Hachaj
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Windows.Forms;
using System.Threading;
using XRtsLibrary.Globals;

namespace Server
{
   public partial class Form1 : Form
   {

      #region Fields

      private readonly TCPServer _server;
      private readonly int _port = GameParameters.Port;

      #endregion

      #region Delegates

      private delegate void ListAddDelegate(object value);
      private delegate void ListRemoveDelegate(object value);

      #endregion

      #region Constructors

      public Form1()
      {
         InitializeComponent();

         _server = new TCPServer(_port);
         var thread = new Thread(_server.StartServer);

         thread.Start();
         _server.Disconnection += Dis;
         _server.NewClient += NewC;
      }

      #endregion

      #region Methods

      private void ListAdd(object value)
      {
         if (ClientsList.InvokeRequired)
         {
            // This is a worker thread so delegate the task.
            ClientsList.Invoke(new ListAddDelegate(ListAdd), value);
         }
         else
         {
            // This is the UI thread so perform the task.
            ClientsList.Items.Add(value);
         }
      }

      private void ListRemove(object value)
      {
         try
         {
            if (ClientsList.InvokeRequired)
            {
               // This is a worker thread so delegate the task.
               ClientsList.Invoke(new ListAddDelegate(ListRemove), value);
            }
            else
            {
               // This is the UI thread so perform the task.
               ClientsList.Items.Remove(value);
            }
         }
         catch
         {
            Console.WriteLine("Player couldn't be removed!");
         }
      }

      private void NewC(object sender, HandleClient client)
      {
         ListAdd(client.ToString());
      }

      private void Dis(object sender, HandleClient client)
      {
         ListRemove(client.ToString());
      }

      private void Form1_FormClosing(object sender, FormClosingEventArgs e)
      {
         _server.StopServer();
      }

      private void buttonKick_Click(object sender, EventArgs e)
      {
         if (ClientsList.SelectedIndex >= 0)
         {
            _server.RemoveClient(ClientsList.SelectedIndex);
         }
      }

      #endregion

   }
}