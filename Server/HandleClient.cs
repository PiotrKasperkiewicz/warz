/*
 * Copyright (c) 2013 Tomasz Hachaj
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Net.Sockets;
using System.Threading;
using System.Net;

namespace Server
{
   internal class HandleClient
   {

      #region Fields

      private TcpClient _clientSocket;
      private string _clientNumber;
      private TCPServer _parentServer;
      private bool _stopClient;
      private readonly int _id = 0;

      #endregion

      #region Constructors

      public HandleClient(int id)
      {
         _id = id;
      }

      #endregion

      #region Methods

      public void StartClient(TcpClient inClientSocket, string clientNumber, TCPServer parentServer)
      {
         _clientSocket = inClientSocket;
         _clientNumber = clientNumber;
         _parentServer = parentServer;

         var clientThread = new Thread(Receiver);

         clientThread.Start();
      }

      public void StopClient()
      {
         _clientSocket.Close();
         _stopClient = true;
      }

      private void Receiver()
      {
         try
         {
            while (!_stopClient && CheckConnection(_clientSocket))
            {
               if (_clientSocket.Available > 0)
               {
                  var bytesFrom = new byte[8];                                                    //modified: changed size of byte array (from 85 to 8)
                  var networkStream = _clientSocket.GetStream();

                  networkStream.Read(bytesFrom, 0, 8);                                                    //as same as above...
                  _parentServer.BroadcastMessage(bytesFrom, this);
               }
               Thread.Sleep(1);
            }
         }
         catch
         {
            _parentServer.RemoveClient(this);
         }
         _parentServer.RemoveClient(this);
      }

      private static bool CheckConnection(TcpClient client)
      {
         if (client.Client.Poll(0, SelectMode.SelectRead))
         {
            var buff = new byte[1];

            if (client.Client.Receive(buff, SocketFlags.Peek) == 0)
            {
               // Client disconnected
               return false;
            }
         }
         return true;
      }

      public void SendData(byte[] data)
      {
         var networkStream = _clientSocket.GetStream();

         networkStream.Write(data, 0, data.Length);
         networkStream.Flush();
      }
      
      public override string ToString()
      {
         var result = "Error has occured!";

         try
         {
            result = "(" + _id + ") " + ((IPEndPoint) _clientSocket.Client.RemoteEndPoint).Address.ToString() + ":" + ((IPEndPoint) _clientSocket.Client.RemoteEndPoint).Port.ToString();
         }
         catch
         {
            Console.WriteLine("Exception was thrown!");
         }

         return result;
      }

      #endregion

   }
}