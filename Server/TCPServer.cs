/*
 * Copyright (c) 2013 Tomasz Hachaj
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Net.Sockets;
using System.Collections;
using System.Net;
using XRtsLibrary.Globals;

namespace Server
{
   class TCPServer
   {

      #region Events and Delegates

      public delegate void NewConnection(object sender, HandleClient client);

      public event NewConnection NewClient;

      public delegate void ConnectionBroken(object sender, HandleClient client);

      public event ConnectionBroken Disconnection;

      #endregion

      #region Fields

      private readonly int _port = GameParameters.Port;
      private readonly string _addressIP = GameParameters.AddressIP;
      private bool _stopServer;
      private readonly ArrayList _allClients = new ArrayList();
      private TcpListener _serverSocket;

      #endregion

      #region Constructors

      public TCPServer(int port)
      {
         _port = port;
      }

      #endregion

      #region Methods

      public void RemoveClient(int id)
      {
         if (id < _allClients.Count)
         {
            var handleClient = (HandleClient) _allClients[id];

            RemoveClient(handleClient);
            handleClient.StopClient();
         }
      }

      public void BroadcastMessage(byte[] message, HandleClient except)
      {
         foreach (var client in _allClients)
         {
            var helpClient = (HandleClient) client;

            if (helpClient != except)
               try
               {
                  helpClient.SendData(message);
               }
               catch
               {
                  Console.WriteLine("Exception was thrown: user cannot send data to himself!");
               }
         }
      }

      public void RemoveClient(HandleClient client)
      {
         Disconnection(this, client);
         _allClients.Remove(client);
      }

      public void StartServer()
      {
         _serverSocket = new TcpListener(IPAddress.Parse(_addressIP), _port);

         var counter = 0;
         var count = 0;

         _serverSocket.Start();

         try
         {
            while (!_stopServer)
            {
               counter += 1;
               var clientSocket = _serverSocket.AcceptTcpClient();
               Console.WriteLine(" >> " + "Client number:" + Convert.ToString(counter) + " started!");

               var client = new HandleClient(count);

               count++;
               _allClients.Add(client);
               client.StartClient(clientSocket, Convert.ToString(counter), this);
               NewClient(this, client);
            }
         }
         catch
         {
            Console.WriteLine("Exception was thrown!");
         }
      }

      public void StopServer()
      {
         _stopServer = true;

         foreach (var client in _allClients)
         {
            var helpClient = (HandleClient) client;

            helpClient.StopClient();
         }
         _serverSocket.Stop();
      }

      #endregion

   }
}