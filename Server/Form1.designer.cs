namespace Server
{
   partial class Form1
   {

      #region Fields

      private System.ComponentModel.IContainer components = null;
      private System.Windows.Forms.ListBox ClientsList;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Button buttonKick;
      public System.Windows.Forms.Label label1;

      #endregion

      #region Methods

      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }

         base.Dispose(disposing);
      }

      #endregion

      #region Windows Form Designer

      private void InitializeComponent()
      {
         ClientsList = new System.Windows.Forms.ListBox();
         groupBox1 = new System.Windows.Forms.GroupBox();
         buttonKick = new System.Windows.Forms.Button();
         label1 = new System.Windows.Forms.Label();
         groupBox1.SuspendLayout();
         SuspendLayout();
         // 
         // ClientsList
         // 
         ClientsList.Anchor =
         (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
           | System.Windows.Forms.AnchorStyles.Right);
         ClientsList.FormattingEnabled = true;
         ClientsList.Location = new System.Drawing.Point(6, 19);
         ClientsList.Name = "ClientsList";
         ClientsList.Size = new System.Drawing.Size(370, 290);
         ClientsList.TabIndex = 0;
         // 
         // groupBox1
         // 
         groupBox1.Anchor =
         (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
           | System.Windows.Forms.AnchorStyles.Right);
         groupBox1.Controls.Add(ClientsList);
         groupBox1.Location = new System.Drawing.Point(12, 12);
         groupBox1.Name = "groupBox1";
         groupBox1.Size = new System.Drawing.Size(382, 313);
         groupBox1.TabIndex = 1;
         groupBox1.TabStop = false;
         groupBox1.Text = "Players list";
         // 
         // buttonKick
         // 
         buttonKick.Location = new System.Drawing.Point(18, 331);
         buttonKick.Name = "buttonKick";
         buttonKick.Size = new System.Drawing.Size(100, 23);
         buttonKick.TabIndex = 1;
         buttonKick.Text = "Kick out player";
         buttonKick.UseVisualStyleBackColor = true;
         buttonKick.Click += new System.EventHandler(buttonKick_Click);
         // 
         // label1
         // 
         label1.AutoSize = true;
         label1.Location = new System.Drawing.Point(159, 335);
         label1.Name = "label1";
         label1.Size = new System.Drawing.Size(53, 13);
         label1.TabIndex = 17;
         //label1.Text = "Players: 0";
         label1.Text = "Players: " + ClientsList.Items.Count;
         // 
         // Form1
         // 
         AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         ClientSize = new System.Drawing.Size(406, 357);
         Controls.Add(label1);
         Controls.Add(buttonKick);
         Controls.Add(groupBox1);
         FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
         Name = "Form1";
         Text = "WarZ Server";
         FormClosing += new System.Windows.Forms.FormClosingEventHandler(Form1_FormClosing);
         groupBox1.ResumeLayout(false);
         ResumeLayout(false);
         PerformLayout();
      }

      #endregion

   }
}