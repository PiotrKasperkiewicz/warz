WarZ is RTS Game (Real-Time-Strategy), this is builded in Visual Studio 2010 using framework XNA Game Studio 4.0.

The game takes place in Azeroth's land, before first conflict between Orcs and Humans...
Here, only humans fighting against themselves : to conquer whole kingdom!

__________________________________________________________________________________________________________________

Control keys

MENU:

Left Mouse Button: chooses selected option

Arrows keys: changes selection

BATTLE SCREEN:

Left Mouse Button: select units (single click or frame selection)/deselect units

Right Mouse Button: move units

Arrow Up: move camera to up

Arrow Down: move camera to down

Arrow Left: move camera to left

Arrow Right: move camera to right

C: Centers view on unit

F: Switches camera mode (Free/Follow)

A: Move unit to left

W: Move unit to up

S: Move unit to down

D: Move unit to right

Remember to set code execution for project "Server" AND "WarZ" in compilator options. For second player, just run second instance of "WarZ" project.
__________________________________________________________________________________________________________________

The other informations soon...