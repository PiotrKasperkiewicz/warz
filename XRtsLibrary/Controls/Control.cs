﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XRtsLibrary.Controls
{
    public abstract class Control
   {

      #region Fields

      protected string name;
      protected string text;
      protected Vector2 size;
      protected Vector2 position;
      protected object value;
      protected bool hasFocus;
      protected bool enabled;
      protected bool visible;
      protected bool tabStop;
      protected SpriteFont spriteFont;
      protected Color color;
      protected string type;
   //Added
      protected Rectangle area;
      protected Texture2D leftTexture;
      protected Texture2D rightTexture;
      protected Rectangle leftTextureArea;
      protected Rectangle rightTextureArea;

      #endregion

      #region Events

      public event EventHandler Selected;

      #endregion

      #region Properties

      public string Name
      {
         get { return name; }
         set { name = value; }
      }

      public string Text
      {
         get { return text; }
         set { text = value; }
      }

      public Vector2 Size
      {
         get { return size; }
         set { size = value; }
      }

      public Vector2 Position
      {
         get { return position; }
         set
         {
            position = value;
            position.Y = (int)position.Y;
         }
      }

      public object Value
      {
         get { return value; }
         set { this.value = value; }
      }

      public bool HasFocus
      {
         get { return hasFocus; }
         set { hasFocus = value; }
      }

      public bool Enabled
      {
         get { return enabled; }
         set { enabled = value; }
      }

      public bool Visible
      {
         get { return visible; }
         set { visible = value; }
      }

      public bool TabStop
      {
         get { return tabStop; }
         set { tabStop = value; }
      }

      public SpriteFont SpriteFont
      {
         get { return spriteFont; }
         set { spriteFont = value; }
      }

      public Color Color
      {
         get { return color; }
         set { color = value; }
      }

      public string Type
      {
         get { return type; }
         set { type = value; }
      }
      //Added
      public Rectangle Area
      {
         get { return area; }
         set { area = value; }
      }

      public Rectangle LeftTextureArea
      {
         get { return leftTextureArea; }
         set { leftTextureArea = value; }
      }

      public Rectangle RightTextureArea
      {
         get { return rightTextureArea; }
         set { rightTextureArea = value; }
      }

      #endregion

      #region Constructors

      public Control()
      {
         Color = Color.White;
         Enabled = true;
         Visible = true;
         SpriteFont = ControlManager.SpriteFont;
      }

      #endregion

      #region Abstract Methods

      public abstract void Update(GameTime gameTime);
      public abstract void Draw(SpriteBatch spriteBatch);
      public abstract void HandleInput(PlayerIndex playerIndex);

      #endregion

      #region Virtual Methods

      protected virtual void OnSelected(EventArgs e)
      {
         if (Selected != null)
         {
            Selected(this, e);
         }
      }

      #endregion

   }
}