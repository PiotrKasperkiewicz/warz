﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XRtsLibrary.Globals;

namespace XRtsLibrary.Controls
{
    public class LeftRightSelector : Control
   {

      #region Events

      public event EventHandler SelectionChanged;

      #endregion

      #region Fields

      List<string> items = new List<string>();

      //Texture2D leftTexture;   modified
      //Texture2D rightTexture;
      Texture2D stopTexture;

      Color selectedColor = Color.Red;
      int selectedItem;
      int maxItemWidth;
      int maxItemHeight;      //added

      #endregion

      #region Properties

      public Color SelectedColor
      {
         get { return selectedColor; }
         set { selectedColor = value; }
      }

      public int SelectedIndex
      {
         get { return selectedItem; }
         set { selectedItem = (int)MathHelper.Clamp(value, 0f, items.Count); }
      }

      public string SelectedItem
      {
         get { return Items[selectedItem]; }
      }

      public List<string> Items
      {
         get { return items; }
      }

      //Added
      public int MaxItemWidth
      {
         get { return maxItemWidth; }
         set { maxItemWidth = value; }
      }

      public int MaxItemHeight
      {
         get { return maxItemHeight; }
         set { maxItemHeight = value; }
      }

      #endregion

      #region Constructors

      public LeftRightSelector(Texture2D leftArrow, Texture2D rightArrow, Texture2D stop)
      {
         leftTexture = leftArrow;
         rightTexture = rightArrow;
         stopTexture = stop;
         TabStop = true;
         Color = Color.White;
         //Added
         MaxItemWidth = 0;
         MaxItemHeight = 0;
      }

      #endregion

      #region Methods

      //public void SetItems(string[] items, int maxWidth)  modified to below
      public void SetItems(string[] items)
      {
         this.items.Clear();
         Vector2 currentSize = new Vector2();   //added

         /* modified
         foreach (string s in items)
            this.items.Add(s);
         */

         foreach (string s in items)
         {
            this.items.Add(s);
            currentSize = this.spriteFont.MeasureString(s);

            if (currentSize.X > MaxItemWidth)
            {
               MaxItemWidth = (int)currentSize.X;
            }

            if (MaxItemHeight == 0)
            {
               MaxItemHeight = (int)currentSize.Y;
            }
         }

         Size = new Vector2(MaxItemWidth, MaxItemHeight);   //added
         //maxItemWidth = maxWidth;    modified
      }

      protected void OnSelectionChanged()
      {
         if (SelectionChanged != null)
         {
            SelectionChanged(this, null);
         }
      }

      #endregion

      #region Abstract Methods

      public override void HandleInput(PlayerIndex playerIndex)
      {
         if (items.Count == 0)
            return;

         if (InputHandler.ButtonReleased(Buttons.LeftThumbstickLeft, playerIndex) ||
            InputHandler.ButtonReleased(Buttons.DPadLeft, playerIndex) ||
            InputHandler.KeyReleased(Keys.Left) || InputHandler.MouseClickedOnArea(leftTextureArea))      //modified: added last inputHandler
         {
            selectedItem--;

            if (selectedItem < 0)
               selectedItem = 0;

            OnSelectionChanged();
         }

         if (InputHandler.ButtonReleased(Buttons.LeftThumbstickRight, playerIndex) ||
            InputHandler.ButtonReleased(Buttons.DPadRight, playerIndex) ||
            InputHandler.KeyReleased(Keys.Right) || InputHandler.MouseClickedOnArea(rightTextureArea))  //modified: added last inputHandler
         {
            selectedItem++;

            if (selectedItem >= items.Count)
               selectedItem = items.Count - 1;

            OnSelectionChanged();
         }
      }

      public override void Update(GameTime gameTime)
      {
      }

      public override void Draw(SpriteBatch spriteBatch)
      {
         Vector2 drawTo = position;

         if (selectedItem != 0)
            spriteBatch.Draw(leftTexture, drawTo, Color.White);
         else
            spriteBatch.Draw(stopTexture, drawTo, Color.White);

         LeftTextureArea = PictureHandler.MeasureArea(drawTo, leftTexture);   //added

         /*    modified to below
         drawTo.X += leftTexture.Width + 5f;
         float itemWidth = spriteFont.MeasureString(items[selectedItem]).X;
         float offset = (maxItemWidth - itemWidth) / 2;

         drawTo.X += offset;
         */

         drawTo.X += leftTexture.Width;

         if (hasFocus)
            spriteBatch.DrawString(spriteFont, items[selectedItem], drawTo, selectedColor);
         else
            spriteBatch.DrawString(spriteFont, items[selectedItem], drawTo, Color);

         //drawTo.X += -1 * offset + maxItemWidth + 5f;  modified to below
         drawTo.X += MaxItemWidth + 5f;

         if (selectedItem != items.Count - 1)
            spriteBatch.Draw(rightTexture, drawTo, Color.White);
         else
            spriteBatch.Draw(stopTexture, drawTo, Color.White);

         RightTextureArea = PictureHandler.MeasureArea(drawTo, rightTexture);    //added
      }

      #endregion

   }
}