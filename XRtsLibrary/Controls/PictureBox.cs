﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XRtsLibrary.Controls
{
    public class PictureBox : Control
   {

      #region Fields

      Texture2D image;
      Rectangle sourceRect;
      Rectangle destRect;

      #endregion

      #region Properties

      public Texture2D Image
      {
         get { return image; }
         set { image = value; }
      }

      public Rectangle SourceRectangle
      {
         get { return sourceRect; }
         set { sourceRect = value; }
      }

      public Rectangle DestinationRectangle
      {
         get { return destRect; }
         set { destRect = value; }
      }

      #endregion

      #region Constructors

      public PictureBox(Texture2D image, Rectangle destination)
      {
         Image = image;
         DestinationRectangle = destination;
         SourceRectangle = new Rectangle(0, 0, image.Width, image.Height);
         //Color = Color.White;  modified
      }

      public PictureBox(Texture2D image, Rectangle destination, Rectangle source)
      {
         Image = image;
         DestinationRectangle = destination;
         SourceRectangle = source;
         //Color = Color.White;     modified
      }

      #endregion

      #region Abstract Methods

      public override void Update(GameTime gameTime)
      {
      }

      public override void Draw(SpriteBatch spriteBatch)
      {
         //spriteBatch.Draw(image, destRect, sourceRect, Color);  modified to below
         spriteBatch.Draw(image, destRect, Color);
      }

      public override void HandleInput(PlayerIndex playerIndex)
      {
      }

      #endregion

      #region Picture Box Methods

      public void SetPosition(Vector2 newPosition)
      {
         destRect = new Rectangle(
            (int)newPosition.X,
            (int)newPosition.Y,
            sourceRect.Width,
            sourceRect.Height
         );
      }

      #endregion

   }
}