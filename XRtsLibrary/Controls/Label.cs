﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XRtsLibrary.Controls
{
    public class Label : Control
   {

      #region Constructors

      public Label()
      {
         tabStop = false;
      }

      #endregion

      #region Abstract Methods

      public override void Update(GameTime gameTime)
      {
      }

      public override void Draw(SpriteBatch spriteBatch)
      {
         spriteBatch.DrawString(SpriteFont, Text, Position, Color);
      }

      public override void HandleInput(PlayerIndex playerIndex)
      {
      }

      #endregion

   }
}