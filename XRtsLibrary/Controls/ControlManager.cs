﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XRtsLibrary.Globals;

namespace XRtsLibrary.Controls
{
    public class ControlManager : List<Control>
   {

      #region Fields

      //int selectedControl = 0;
      int selectedControl;

      static SpriteFont spriteFont;

      #endregion

      #region Properties

      public static SpriteFont SpriteFont
      {
         get { return spriteFont; }
      }

      #endregion

      #region Events

      public event EventHandler FocusChanged;

      #endregion

      #region Constructors

      public ControlManager(SpriteFont spriteFont)
         : base()
      {
         ControlManager.spriteFont = spriteFont;
      }

      public ControlManager(SpriteFont spriteFont, int capacity)
         : base(capacity)
      {
         ControlManager.spriteFont = spriteFont;
      }

      public ControlManager(SpriteFont spriteFont, IEnumerable<Control> collection) :
         base(collection)
      {
         ControlManager.spriteFont = spriteFont;
      }

      #endregion

      #region Methods

      public void NextControl()
      {
         if (Count == 0)
            return;

         int currentControl = selectedControl;

         this[selectedControl].HasFocus = false;

         do
         {
            selectedControl++;

            if (selectedControl == Count)
               selectedControl = 0;

            if (this[selectedControl].TabStop && this[selectedControl].Enabled)
            {
               if (FocusChanged != null)
                  FocusChanged(this[selectedControl], null);

               break;
            }

         } while (currentControl != selectedControl);

         this[selectedControl].HasFocus = true;
      }

      public void PreviousControl()
      {
         if (Count == 0)
            return;

         int currentControl = selectedControl;

         this[selectedControl].HasFocus = false;

         do
         {
            selectedControl--;

            if (selectedControl < 0)
               selectedControl = Count - 1;

            if (this[selectedControl].TabStop && this[selectedControl].Enabled)
            {
               if (FocusChanged != null)
                  FocusChanged(this[selectedControl], null);

               break;
            }

         } while (currentControl != selectedControl);

         this[selectedControl].HasFocus = true;
      }
      //Added
      public void SelectedControl(Control c)
      {
         if (Count == 0 || c.Area == this[selectedControl].Area)
         {
            return;
         }

         int currentControl = selectedControl;

         while (c.Area != this[selectedControl].Area)
         {
            selectedControl++;
            if (selectedControl == Count)
            {
               selectedControl = 0;
            }
         }

         this[currentControl].HasFocus = false;

         if (FocusChanged != null)
         {
            FocusChanged(this[selectedControl], null);
         }

         this[selectedControl].HasFocus = true;
      }

      public void MeasureControls()
      {
         foreach (Control c in this)
         {
            c.Area = PictureHandler.MeasureArea(c);
         }
      }

      public void Update(GameTime gameTime, PlayerIndex playerIndex)
      {
         if (Count == 0)
            return;

         foreach (Control c in this)
         {
            if (c.Enabled)
               c.Update(gameTime);

            if (c.HasFocus)
               c.HandleInput(playerIndex);
            //Added
            if (c is LeftRightSelector)
            {
               if (InputHandler.Mouseover(c.Area, c.LeftTextureArea, c.RightTextureArea) && c.TabStop && c.Enabled && c.Area != this[selectedControl].Area)
               {
                  SelectedControl(c);
               }
            }

            if (InputHandler.Mouseover(c.Area) && c.TabStop && c.Enabled && c.Area != this[selectedControl].Area)
            {
               SelectedControl(c);
            }
         }

         if (InputHandler.ButtonPressed(Buttons.LeftThumbstickUp, playerIndex) ||
            InputHandler.ButtonPressed(Buttons.DPadUp, playerIndex) ||
            InputHandler.KeyPressed(Keys.Up))
            PreviousControl();

         if (InputHandler.ButtonPressed(Buttons.LeftThumbstickDown, playerIndex) ||
            InputHandler.ButtonPressed(Buttons.DPadDown, playerIndex) ||
            InputHandler.KeyPressed(Keys.Down))
            NextControl();
      }

      public void Draw(SpriteBatch spriteBatch)
      {
         foreach (Control c in this)
         {
            if (c.Visible)
               c.Draw(spriteBatch);
         }
      }
      
      #endregion

   }
}