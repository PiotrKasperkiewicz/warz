﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace XRtsLibrary.Controls
{
    public class LinkLabel : Control
   {
      
      #region Fields

      Color selectedColor = Color.Red;

      #endregion

      #region Properties

      public Color SelectedColor
      {
         get { return selectedColor; }
         set { selectedColor = value; }
      }

      #endregion

      #region Constructors

      public LinkLabel()
      {
         TabStop = true;
         HasFocus = false;
         Position = Vector2.Zero;
      }

      #endregion

      #region Abstract Methods

      public override void Update(GameTime gameTime)
      {
      }

      public override void Draw(SpriteBatch spriteBatch)
      {
         if (hasFocus)
            spriteBatch.DrawString(SpriteFont, Text, Position, selectedColor);
         else
            spriteBatch.DrawString(SpriteFont, Text, Position, Color);
      }

      public override void HandleInput(PlayerIndex playerIndex)
      {
         if (!HasFocus)
            return;
         
         if (InputHandler.KeyReleased(Keys.Enter) ||
            InputHandler.ButtonReleased(Buttons.A, playerIndex))
            base.OnSelected(null);

         //added
         if (InputHandler.LeftMouseReleased() && InputHandler.Mouseover(base.Area) && base.HasFocus)
            base.OnSelected(null);
      }

      #endregion

   }
}