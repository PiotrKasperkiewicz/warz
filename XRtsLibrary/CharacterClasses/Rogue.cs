﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */


namespace RtsLibrary.CharacterClasses
{
    public class Rogue : Entity
   {

      #region Fields

      #endregion

      #region Properties

      #endregion

      #region Constructors

      public Rogue(EntityData entityData)
         : base(entityData)
      {
      }

      #endregion

      #region Methods

      #endregion

   }
}