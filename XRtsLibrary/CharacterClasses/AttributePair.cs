﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */


namespace RtsLibrary.CharacterClasses
{
    public class AttributePair
   {

      #region Fields

      int currentValue;
      int maximumValue;

      #endregion

      #region Properties

      public int CurrentValue
      {
         get { return currentValue; }
      }

      public int MaximumValue
      {
         get { return maximumValue; }
      }

      public static AttributePair Zero
      {
         get { return new AttributePair(); }
      }

      #endregion

      #region Constructors

      private AttributePair()
      {
         currentValue = 0;
         maximumValue = 0;
      }

      public AttributePair(int maxValue)
      {
         currentValue = maxValue;
         maximumValue = maxValue;
      }

      #endregion

      #region Methods

      public void Heal(ushort value)
      {
         currentValue += value;

         if (currentValue > maximumValue)
            currentValue = maximumValue;
      }

      public void Damage(ushort value)
      {
         currentValue -= value;

         if (currentValue < 0)
            currentValue = 0;
      }

      public void SetCurrent(int value)
      {
         currentValue = value;

         if (currentValue > maximumValue)
            currentValue = maximumValue;
      }

      public void SetMaximum(int value)
      {
         maximumValue = value;

         if (currentValue > maximumValue)
            currentValue = maximumValue;
      }

      #endregion

   }
}