﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */


namespace RtsLibrary.CharacterClasses
{
    public class EntityData
   {

      #region Fields

      //public string ClassName; modified to below
      public string EntityName;

      public int Strength;
      public int Dexterity;
      public int Cunning;
      public int Willpower;
      public int Magic;
      public int Constitution;

      public string HealthFormula;
      public string StaminaFormula;
      public string MagicFormula;

      #endregion

      #region Constructors

      private EntityData()
      {
      }

      #endregion

      #region Static Methods

      public static void ToFile(string filename)
      {
      }

      public static EntityData FromFile(string filename)
      {
         EntityData entity = new EntityData();
         return entity;
      }

      #endregion

   }
}