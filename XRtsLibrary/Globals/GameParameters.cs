﻿using Microsoft.Xna.Framework;

namespace XRtsLibrary.Globals
{
    public static class GameParameters
   {

      #region Fields

      private static readonly bool _noSound = false;
      private static readonly float _masterVolume = 0.1f;
      //enum CameraMode { Free, Follow }
      //CameraMode cameraMode = CameraMode.Free;
      private static readonly int _tileWidthPixels = 32;
      private static readonly int _tileHeightPixels = 32;
      private static readonly int _spriteWidthPixels = 32;
      private static readonly int _spriteHeightPixels = 32;
      private static readonly int _mapWidth = 32;  //both in tiles!
      private static readonly int _mapHeight = 32;
      private static readonly int _port = 14242;
      private static readonly string _addressIP = "127.0.0.1";
      private static readonly Point _playerOneStart = new Point(5, 1);
      private static readonly Point _playerTwoStart = new Point(13, 21);
      //private static Point[] _playerOneStartPoints = new Point[] { new Point { X = 5, Y = 1 }, new Point { X = 8, Y = 1 }, new Point { X = 11, Y = 1 }, new Point { X = 14, Y = 1 } };    //ADDED
      //private static Point[] _playerTwoStartPoints = new Point[] { new Point { X = 21, Y = 22 }, new Point { X = 24, Y = 22 }, new Point { X = 27, Y = 22 }, new Point { X = 30, Y = 22 } };    //ADDED

      #endregion

      #region Properties
      /*
      public static Point[] PlayerOneStart                                                                 //ADDED
      {
         get { return _playerOneStart; }
         set { _playerOneStart = value; }
      }

      public static Point[] PlayerTwoStart
      {
         get { return _playerTwoStart; }
         set { _playerTwoStart = value; }
      }
      */
      public static bool NoSound
      {
         get { return _noSound; }
      }

      public static float MasterVolume
      {
         get { return _masterVolume; }
      }
      /*
      public static CameraMode CameraMode
      {
         get { return cameraMode; }
      }
      */

      public static int TileWidthPixels
      {
         get { return _tileWidthPixels; }
      }

      public static int TileHeightPixels
      {
         get { return _tileHeightPixels; }
      }

      public static int SpriteWidthPixels
      {
         get { return _spriteWidthPixels; }
      }

      public static int SpriteHeightPixels
      {
         get { return _spriteHeightPixels; }
      }

      public static int MapWidth
      {
         get { return _mapWidth; }
      }

      public static int MapHeight
      {
         get { return _mapHeight; }
      }

      public static int Port
      {
         get { return _port; }
      }

      public static string AddressIP
      {
         get { return _addressIP; }
      }

      public static Point PlayerOneStart
      {
         get { return _playerOneStart; }
      }
      public static Point PlayerTwoStart
      {
         get { return _playerTwoStart; }
      }

      #endregion

   }
}