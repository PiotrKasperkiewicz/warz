﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace XRtsLibrary.Globals
{
    public class SoundHandler
   {

      #region Fields

      private static SoundEffect _menuMusic;
      private static SoundEffect _battleMusic;

      #endregion

      #region Properties

      public static SoundEffect MenuMusic
      {
         get { return _menuMusic; }
         set { _menuMusic = value; }
      }

      public static SoundEffect BattleMusic
      {
         get { return _battleMusic; }
         set { _battleMusic = value; }
      }

      #endregion

      #region XNA Methods

      public static void LoadContent(Game game)
      {
         //_menuMusic = game.Content.Load<SoundEffect>(@"Audio\theme");           //soundEffect - only WAV!
         //_battleMusic = game.Content.Load<SoundEffect>(@"Audio\battle");
      }
      
      #endregion

   }
}
