﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRtsLibrary.Controls;

namespace XRtsLibrary.Globals
{
    public static class PictureHandler
   {

      #region Methods

      public static Rectangle MeasureArea(int x, int y, int width, int height)
      {
         return new Rectangle(x, y, width, height);
      }

      public static Rectangle MeasureArea(Control c)
      {
         return new Rectangle((int)c.Position.X, (int)c.Position.Y, (int)c.Size.X, (int)c.Size.Y);
      }

      public static Rectangle MeasureArea(Vector2 vector, Texture2D texture)
      {
         return new Rectangle((int)vector.X, (int)vector.Y, (int)texture.Width, (int)texture.Height);
      }

      #endregion

   }
}
