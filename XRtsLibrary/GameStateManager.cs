/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;

namespace XRtsLibrary
{
    public class GameStateManager : GameComponent
   {

      #region Events

      public event EventHandler OnStateChange;

      #endregion

      #region Fields

      Stack<GameState> gameStates = new Stack<GameState>();

      const int startDrawOrder = 5000;
      const int drawOrderInc = 100;
      int drawOrder;

      #endregion

      #region Properties

      public GameState CurrentState
      {
         get { return gameStates.Peek(); }
      }

      #endregion

      #region Constructors

      public GameStateManager(Game game)
         : base(game)
      {
         drawOrder = startDrawOrder;
      }

      #endregion

      #region XNA Methods

      public override void Initialize()
      {
         base.Initialize();
      }

      public override void Update(GameTime gameTime)
      {
         base.Update(gameTime);
      }

      #endregion

      #region Methods

      public void PopState()
      {
         if (gameStates.Count > 0)
         {
            RemoveState();
            drawOrder -= drawOrderInc;

            if (OnStateChange != null)
               OnStateChange(this, null);
         }
      }

      void RemoveState()
      {
         GameState State = gameStates.Peek();
         OnStateChange -= State.StateChange;
         Game.Components.Remove(State);
         gameStates.Pop();
      }

      public void PushState(GameState newState)
      {
         drawOrder += drawOrderInc;
         newState.DrawOrder = drawOrder;

         AddState(newState);

         if (OnStateChange != null)
            OnStateChange(this, null);
      }

      void AddState(GameState newState)
      {
         gameStates.Push(newState);

         Game.Components.Add(newState);

         OnStateChange += newState.StateChange;
      }

      public void ChangeState(GameState newState)
      {
         while (gameStates.Count > 0)
            RemoveState();

         newState.DrawOrder = startDrawOrder;
         drawOrder = startDrawOrder;

         AddState(newState);

         if (OnStateChange != null)
            OnStateChange(this, null);
      }

      #endregion

   }
}