﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XRtsLibrary.WorldClasses
{
    public class World
   {

      #region Graphic Fields

      Rectangle screenRect;

      #endregion

      #region Properties

      public Rectangle ScreenRectangle
      {
         get { return screenRect; }
      }

      #endregion

      #region Constructors

      public World(Rectangle screenRectangle)
      {
         screenRect = screenRectangle;
      }

      #endregion

      #region Methods

      public void Update(GameTime gameTime)
      {
      }

      public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
      {
      }

      #endregion

   }
}