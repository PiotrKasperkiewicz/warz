/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;

namespace XRtsLibrary
{
    public abstract partial class GameState : DrawableGameComponent
   {

      #region Fields

      List<GameComponent> childComponents;
      GameState tag;
      protected GameStateManager StateManager;

      #endregion

      #region Properties

      public List<GameComponent> Components
      {
         get { return childComponents; }
      }

      public GameState Tag
      {
         get { return tag; }
      }

      #endregion

      #region Constructors

      public GameState(Game game, GameStateManager manager)
         : base(game)
      {
         StateManager = manager;
         childComponents = new List<GameComponent>();
         tag = this;
      }

      #endregion

      #region XNA Drawable Game Component Methods

      public override void Initialize()
      {
         base.Initialize();
      }

      public override void Update(GameTime gameTime)
      {
         foreach (GameComponent component in childComponents)
         {
            if (component.Enabled)
               component.Update(gameTime);
         }

         base.Update(gameTime);
      }

      public override void Draw(GameTime gameTime)
      {
         DrawableGameComponent drawComponent;

         foreach (GameComponent component in childComponents)
         {
            if (component is DrawableGameComponent)
            {
               drawComponent = component as DrawableGameComponent;

               if (drawComponent.Visible)
                  drawComponent.Draw(gameTime);
            }
         }

         base.Draw(gameTime);
      }

      #endregion

      #region Game State Methods

      internal protected virtual void StateChange(object sender, EventArgs e)
      {
         if (StateManager.CurrentState == Tag)
            Show();
         else
            Hide();
      }

      protected virtual void Show()
      {
         Visible = true;
         Enabled = true;

         foreach (GameComponent component in childComponents)
         {
            component.Enabled = true;

            if (component is DrawableGameComponent)
               ((DrawableGameComponent)component).Visible = true;
         }
      }

      protected virtual void Hide()
      {
         Visible = false;
         Enabled = false;

         foreach (GameComponent component in childComponents)
         {
            component.Enabled = false;

            if (component is DrawableGameComponent)
               ((DrawableGameComponent)component).Visible = false;
         }
      }

      #endregion

   }
}