﻿/* Original author:  Jatin Thakur
 * Source website: http://www.codebytes.in/2015/02/a-shortest-path-finding-algorithm.html
 * Original title: "A* Shortest Path Finding Algorithm Implementation in Java"
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using XRtsLibrary.TileEngine;
using XRtsLibrary.Globals;

namespace XRtsLibrary.SpriteClasses
{
   public class AStar
   {

      #region Fields

      private const int VhCost = 10;
      private const int DiagonalCost = 14;
      private static readonly int MapWidth = GameParameters.MapWidth;
      private static readonly int MapHeight = GameParameters.MapHeight;

      //Blocked cells are just null Cell values in grid
      private static Cell[,] _grid = new Cell[MapWidth, MapHeight];

      private static PriorityQueue<Cell> _open;

      private static bool[,] _closed = new bool[MapWidth, MapHeight];
      private static int _startI, _startJ;
      private static int _endI, _endJ;

      #endregion

      public class Cell
      {

         #region Fields

         public int HeuristicCost = 0; //Heuristic cost -> H
         public int FinalCost = 0; //F = G + H
         public readonly int i;
         public readonly int j;
         public Cell Parent;

         #endregion

         #region Constructors

         public Cell(int i, int j)
         {
            this.i = i;
            this.j = j;
         }

         #endregion

         #region Methods

         public override string ToString()
         {
            return "[" + i + ", " + j + "]";
         }

         public int Compare(Cell cellToCompare)
         {
            return FinalCost < cellToCompare.FinalCost ? -1 : FinalCost > cellToCompare.FinalCost ? 1 : 0;
         }

         #endregion

      }

      #region Methods

      private static void SetStartCell(int i, int j)
      {
         _startI = i;
         _startJ = j;
      }

      private static void SetEndCell(int i, int j)
      {
         _endI = i;
         _endJ = j;
      }

      private static void SetBlocked(int i, int j)
      {
         _grid[i, j] = null;
      }

      private static void CheckAndUpdateCost(Cell current, Cell t, int cost)
      {
         if (t == null || _closed[t.i, t.j])
            return;

         var tFinalCost = t.HeuristicCost + cost;
         var inOpen = _open.Contains(t);

         if (!inOpen || tFinalCost < t.FinalCost)
         {
            t.FinalCost = tFinalCost;
            t.Parent = current;

            if (!inOpen)
               _open.Enqueue(t);
         }
      }

      private static void FindPath(Point startPoint, Point endPoint)
      {
         //add the start location to open list.
         _open.Enqueue(_grid[startPoint.X, startPoint.Y]);

         while (true)
         {
            var current = _open.Dequeue();

            if (current == null)
               break;

            _closed[current.i, current.j] = true;

            if (current.Equals(_grid[endPoint.X, endPoint.Y]))
            {
               return;
            }

            Cell t;

            if (current.i - 1 >= 0)
            {
               t = _grid[current.i - 1, current.j];
               CheckAndUpdateCost(current, t, current.FinalCost + VhCost);

               if (current.j - 1 >= 0)
               {
                  t = _grid[current.i - 1, current.j - 1];
                  CheckAndUpdateCost(current, t, current.FinalCost + DiagonalCost);
               }
               if (current.j + 1 < _grid.GetLength(0))
               {
                  t = _grid[current.i - 1, current.j + 1];
                  CheckAndUpdateCost(current, t, current.FinalCost + DiagonalCost);
               }
            }

            if (current.j - 1 >= 0)
            {
               t = _grid[current.i, current.j - 1];
               CheckAndUpdateCost(current, t, current.FinalCost + VhCost);
            }

            if (current.j + 1 < _grid.GetLength(0))
            {
               t = _grid[current.i, current.j + 1];
               CheckAndUpdateCost(current, t, current.FinalCost + VhCost);
            }

            //if (current.i + 1 < grid.Length)    //modified: out of range exception...
            if (current.i + 1 < _grid.GetLength(0))
            {
               t = _grid[current.i + 1, current.j];
               CheckAndUpdateCost(current, t, current.FinalCost + VhCost);

               if (current.j - 1 >= 0)
               {
                  t = _grid[current.i + 1, current.j - 1];
                  CheckAndUpdateCost(current, t, current.FinalCost + DiagonalCost);
               }

               if (current.j + 1 < _grid.GetLength(0))
               {
                  t = _grid[current.i + 1, current.j + 1];
                  CheckAndUpdateCost(current, t, current.FinalCost + DiagonalCost);
               }
            }
         }
      }

      public static IEnumerable<Point> GetPath(Point start, Point end, MapLayer collisionsMap)
      {
         _grid = new Cell[MapWidth, MapHeight];
         _closed = new bool[MapWidth, MapHeight];
         _open = new PriorityQueue<Cell>();
         SetStartCell(start.X, start.Y);
         SetEndCell(end.X, end.Y);
         var path = new List<Point>();

         //define heuristic cost for every cell on map: 
         for (var i = 0; i < MapWidth; ++i)
         {
            for (var j = 0; j < MapHeight; ++j)
            {
               _grid[i, j] = new Cell(i, j) {HeuristicCost = Math.Abs(i - end.X) + Math.Abs(j - end.Y)};
            }
         }

         _grid[start.X, start.Y].FinalCost = 0;

         //rewrite layer with collision's points
         for (var i = 0; i < collisionsMap.Width; i++)
         {
            for (var j = 0; j < collisionsMap.Height; j++)
            {
               var collisionPoint = collisionsMap.GetTile(i, j);

               if (collisionPoint.Tileset != 0) //tile is checked by number of used source tileset (plain terrain is 0)
                  SetBlocked(i, j);
            }
         }

         FindPath(start, end);

         if (_closed[end.X, end.Y])
         {
            //Trace back the path
            var current = _grid[end.X, end.Y];
            var point = new Point(current.i, current.j);
            path.Add(point);

            while (current.Parent != null)
            {
               point = new Point(current.Parent.i, current.Parent.j);
               path.Add(point);
               current = current.Parent;
            }

            path.Reverse(); //list is reversed according to order of steps from start point to end point

            foreach (var t in path)
            {
               Debug.Write(" -> " + t);
            }

            Debug.WriteLine("");
         }
         else
            Debug.WriteLine("No possible path");

         return path;
      }

      #endregion

   }
}