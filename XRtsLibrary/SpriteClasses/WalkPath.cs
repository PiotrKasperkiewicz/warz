﻿using System;
using System.Collections.Generic;

namespace XRtsLibrary.SpriteClasses
{
   public class WalkPath : ICloneable
   {
      private readonly Queue<WayPoint> _wayPoints = new Queue<WayPoint>();

      public WalkPath(IEnumerable<WayPoint> wayPoints)
      {
         foreach (var wayPoint in wayPoints)
         {
            _wayPoints.Enqueue((WayPoint) wayPoint.Clone());
         }
      }

      private WalkPath()
      {
         _wayPoints = new Queue<WayPoint>();
      }

      public int Count
      {
         get { return _wayPoints.Count; }
      }

      public WayPoint Dequeue()
      {
         return _wayPoints.Count != 0 ? _wayPoints.Dequeue() : null;
      }

      public object Clone()
      {
         var walkPath = new WalkPath();

         foreach (var wayPoint in _wayPoints)
            walkPath._wayPoints.Enqueue((WayPoint) wayPoint.Clone());

         return walkPath;
      }
   }
}