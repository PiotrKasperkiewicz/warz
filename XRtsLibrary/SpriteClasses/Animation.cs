﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using System;
using Microsoft.Xna.Framework;

namespace XRtsLibrary.SpriteClasses
{
    //public enum AnimationKey { Down, Left, Right, Up }  //modified to below:
    public enum AnimationKey { Down, Left, Right, Up , RU, LU, RD, LD }

   public class Animation : ICloneable
   {

      #region Fields

      private readonly Rectangle[] _frames;
      private int _framesPerSecond;
      private TimeSpan _frameLength;
      private TimeSpan _frameTimer;
      private int _currentFrame;
      private int _frameWidth;
      private int _frameHeight;

      #endregion

      #region Properties

      private int FramesPerSecond
      {
         get { return _framesPerSecond; }
         set
         {
            if (value < 1)
               _framesPerSecond = 1;
            else if (value > 60)
               _framesPerSecond = 60;
            else
               _framesPerSecond = value;

            _frameLength = TimeSpan.FromSeconds(1 / (double)_framesPerSecond);
         }
      }

      public Rectangle CurrentFrameRect
      {
         get { return _frames[_currentFrame]; }
      }

      public int CurrentFrame
      {
         get { return _currentFrame; }
         set
         {
            _currentFrame = (int)MathHelper.Clamp(value, 0, _frames.Length - 1);
         }
      }

      public int FrameWidth
      {
         get { return _frameWidth; }
      }

      public int FrameHeight
      {
         get { return _frameHeight; }
      }

      #endregion

      #region Constructors

      public Animation(int frameCount, int frameWidth, int frameHeight, int xOffset, int yOffset)
      {
         _frames = new Rectangle[frameCount];
         _frameWidth = frameWidth;
         _frameHeight = frameHeight;

         for (var i = 0; i < frameCount; i++)
         {
            _frames[i] = new Rectangle(
               xOffset + (frameWidth * i),
               yOffset,
               frameWidth,
               frameHeight
            );
         }
         FramesPerSecond = 5;
         Reset();
      }

      private Animation(Animation animation)
      {
         _frames = animation._frames;
         FramesPerSecond = 5;
      }

      #endregion

      #region Methods

      public void Update(GameTime gameTime)
      {
         _frameTimer += gameTime.ElapsedGameTime;

         if (_frameTimer >= _frameLength)
         {
            _frameTimer = TimeSpan.Zero;
            _currentFrame = (_currentFrame + 1) % _frames.Length;
         }
      }

      private void Reset()
      {
         _currentFrame = 0;
         _frameTimer = TimeSpan.Zero;
      }

      #endregion

      #region Interface Methods

      public object Clone()
      {
         var animationClone = new Animation(this)
         {
            _frameWidth = _frameWidth,
            _frameHeight = _frameHeight
         };

         animationClone.Reset();

         return animationClone;
      }

      #endregion

   }
}