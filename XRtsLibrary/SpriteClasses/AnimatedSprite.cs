﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRtsLibrary.TileEngine;
using XRtsLibrary.Globals;
using System.Linq;

namespace XRtsLibrary.SpriteClasses
{
   public class AnimatedSprite : ICloneable
   {

      #region Fields

      private readonly Dictionary<AnimationKey, Animation> _animations;
      private AnimationKey _currentAnimation;
      private bool _isAnimating;
      private float _speed = 25f;

      private readonly Texture2D _texture;
      private Vector2 _position = Engine.CellToVector(GameParameters.PlayerOneStart);
      private Vector2 _velocity;
      List<Point> Path = new List<Point>();
      Vector2 direction;

      private readonly int _tileWidth = GameParameters.TileWidthPixels;
      private readonly int _tileHeight = GameParameters.TileHeightPixels;

      #endregion

      #region Properties

      public AnimationKey CurrentAnimation
      {
         get { return _currentAnimation; }
         set { _currentAnimation = value; }
      }

      public bool IsAnimating
      {
         get { return _isAnimating; }
         set { _isAnimating = value; }
      }

      public int Width
      {
         get { return _animations[_currentAnimation].FrameWidth; }
      }

      public int Height
      {
         get { return _animations[_currentAnimation].FrameHeight; }
      }

      public float Speed
      {
         get { return _speed; }
         set { _speed = MathHelper.Clamp(_speed, 1.0f, 16.0f); }
      }

      public Vector2 Position
      {
         get { return _position; }
         set { _position = value; }
      }

      public Vector2 Velocity
      {
         get { return _velocity; }
         set
         {
            _velocity = value;

            if (_velocity != Vector2.Zero)
               _velocity.Normalize();
         }
      }

      public Rectangle UnitRectangle
      {
         get { return new Rectangle((int)_position.X, (int)_position.Y, Width, Height); }
      }

      #endregion

      #region Constructors

      public AnimatedSprite(Texture2D sprite, Dictionary<AnimationKey, Animation> animation)
      {
         _texture = sprite;
         _animations = new Dictionary<AnimationKey, Animation>();

         foreach (var key in animation.Keys)
            _animations.Add(key, (Animation)animation[key].Clone());
      }

      #endregion

      #region XNA Methods

      public void Update(GameTime gameTime)
      {
         if (_isAnimating)
            _animations[_currentAnimation].Update(gameTime);

         WalkPath(gameTime);        //here is played animation move, according to found path
      }

      AnimationKey GetAnimationByDirection(Vector2 direction)
      {
         int x = (int)direction.X;
         int y = (int)direction.Y;

         if (y == -1)
            return AnimationKey.Up;
         if (x == 1 && y == -1)
            return AnimationKey.RU;
         if (x == 1)
            return AnimationKey.Right;
         if (x == 1 && y == 1)
            return AnimationKey.RD;
         if (y == 1)
            return AnimationKey.Down;
         if (x == -1 && y == 1)
            return AnimationKey.LD;
         if (x == -1)
            return AnimationKey.Left;
         if (x == -1 && y == -1)
            return AnimationKey.LU;

         return AnimationKey.Down;
      }

      bool DoNextNode()
      {
         Point pathPoint = Path.First();
         float maxDistance = 0.2f;

         Vector2 pathPosition = new Vector2(pathPoint.X * _tileWidth, pathPoint.Y * _tileHeight);

         return Vector2.Distance(pathPosition, Position) < maxDistance;
      }

      void WalkPath(GameTime gt)
      {
         if (Path.Count > 0)
         {
            Vector2 destination = new Vector2(Path.First().X * _tileWidth, Path.First().Y * _tileHeight);
            direction = Vector2.Normalize(destination - Position);

            Position += direction * _speed * (float)gt.ElapsedGameTime.TotalSeconds;

            CurrentAnimation = GetAnimationByDirection(direction);

            if (DoNextNode())
            {
               if (Path.Count == 1)
                  Position = Engine.CellToVector(Path[0]);

               Path.RemoveAt(0);
            }
         }
      }

      public void Move(MapLayer splatter)
      {
         var oldPosition = Position;
         var startPoint = Engine.VectorToCell(oldPosition);
         var cursorPosition = InputHandler.CursorPosition;
         var endPoint = Engine.VectorToCell(cursorPosition);
         var distance = cursorPosition - oldPosition;
         var newPosition = oldPosition + distance;

         Debug.WriteLine("You clicked by right mouse button at tile: {0} with {1} pixels.", endPoint, newPosition);

         var terrainType = splatter.GetTile(endPoint.X, endPoint.Y);

         if (startPoint == endPoint || terrainType.Tileset != 0)
            return;

         Path.Clear();
         Path = AStar.GetPath(startPoint, endPoint, splatter) as List<Point>;
      }

      public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Camera camera)
      {
         spriteBatch.Draw(
            _texture,
            _position,
            _animations[_currentAnimation].CurrentFrameRect,
            Color.White
         );
      }

      public void Draw(GameTime gameTime, SpriteBatch spriteBatch, Camera camera, Vector2 enemyPosition)
      {
         spriteBatch.Draw(
            _texture,
            enemyPosition,
            _animations[_currentAnimation].CurrentFrameRect,
            Color.White
         );
      }

      public void LockToMap()
      {
         _position.X = MathHelper.Clamp(_position.X, 0, TileMap.WidthInPixels - Width);
         _position.Y = MathHelper.Clamp(_position.Y, 0, TileMap.HeightInPixels - Height);
      }

      #endregion

      #region ICloneable Members

      public object Clone()
      {
         var newSprite = new AnimatedSprite(_texture, _animations)
         {
            _position = _position,
            _velocity = _velocity,
            _speed = _speed,
            _currentAnimation = AnimationKey.Down,
            _isAnimating = false
         };

         return newSprite;
      }

      #endregion

   }
}