﻿using System;
using Microsoft.Xna.Framework;

namespace XRtsLibrary.SpriteClasses
{
   public class WayPoint : ICloneable
   {
      private Vector2 _startPoint;
      private Vector2 _stopPoint;
      private Vector2 _velocity;
      private Vector2 _position;
      private TimeSpan _speed;
      private TimeSpan _timer;
      private AnimationKey _animation;

      public Vector2 Position
      {
         get { return _position; }
      }

      public AnimationKey Animation
      {
         get { return _animation; }
      }

      public WayPoint(Vector2 startPoint, Vector2 stopPoint, Vector2 velocity, AnimationKey animation)
      {
         _startPoint = startPoint;
         _stopPoint = stopPoint;
         _velocity = velocity;
         _position = _startPoint;
         _animation = animation;
      }

      public WayPoint(Vector2 startPoint, Vector2 stopPoint, Vector2 velocity, int milliseconds, AnimationKey animation)
      {
         _startPoint = startPoint;
         _stopPoint = stopPoint;
         _velocity = velocity;
         _position = _startPoint;
         _speed = TimeSpan.FromMilliseconds(milliseconds);
         _timer = TimeSpan.Zero;
         _animation = animation;
      }

      private WayPoint()
      {
         _startPoint = Vector2.Zero;
         _stopPoint = Vector2.Zero;
         _velocity = Vector2.Zero;
         _position = _startPoint;
         _speed = TimeSpan.Zero;
         _timer = TimeSpan.Zero;
         _animation = AnimationKey.Up;
      }

      public void Update(GameTime gameTime)
      {
         _timer += gameTime.ElapsedGameTime;

         if (_timer >= _speed)
         {
            _position += (_velocity*(float) _speed.TotalSeconds);
            _timer -= _speed;
         }
      }

      public bool EndReached()
      {
         return Vector2.Distance(_position, _stopPoint) <= 1f;
      }

      public object Clone()
      {
         var wayPoint = new WayPoint
         {
            _startPoint =
            {
               X = _startPoint.X,
               Y = _startPoint.Y
            },
            _stopPoint =
            {
               X = _stopPoint.X,
               Y = _stopPoint.Y
            },
            _velocity =
            {
               X = _velocity.X,
               Y = _velocity.Y
            },
            _position =
            {
               X = _position.X,
               Y = _position.Y
            },
            _speed = _speed,
            _timer = _timer,
            _animation = _animation
         };

         return wayPoint;
      }
   }
}