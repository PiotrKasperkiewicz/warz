﻿using System;
using Microsoft.Xna.Framework;
using XRtsLibrary.TileEngine;

namespace XRtsLibrary.SpriteClasses
{
   public abstract class StartpointCollisionHandler
   {//this class helps in solving problems with collisions at start point (collision are generated at random positions, so it would be nice to place units not over them or in "dead" areas.

      #region Methods

      public static Point CheckStartpoint(Point tile, MapLayer collisionsMap)
      {
         var collisionPoint = collisionsMap.GetTile(tile.X, tile.Y);
         var startPoint = tile;
         var adjacentTile = new AdjacentTile();

         adjacentTile.AddNeighbours(tile, collisionsMap);

         if (collisionPoint.Tileset != 0)                //checking first condition - if unit is at wrong tile
         {
            if (!adjacentTile.IsExit())                  //checking, if exists at least one adjacent tile as free
            {
               Console.WriteLine("I can't move!");
            }
            else
            {
               startPoint = adjacentTile.GetFirstFreeTile();
            }
         }
         else if (!adjacentTile.IsExit())                       //unit stays at correct tile, but is surrounded by obstacles and we find, if exit exists...
               Console.WriteLine("I can't move!");

         return startPoint;
      }

      #endregion

   }
}
