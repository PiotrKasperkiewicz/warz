﻿/* Original author: dr James McCaffrey
 * Source website: https://visualstudiomagazine.com/Articles/2012/11/01/Priority-Queues-with-C.aspx
 * Original title: "Priority Queues with C#"
 */

using System.Collections.Generic;
using static XRtsLibrary.SpriteClasses.AStar;

namespace XRtsLibrary.SpriteClasses
{
    public class PriorityQueue<T>
   {

      #region Fields

      readonly List<Cell> _data;

      #endregion

      #region Constructors

      public PriorityQueue()
      {
         this._data = new List<Cell>();
      }

      #endregion

      #region Methods

      public void Enqueue(Cell item)
      {
         _data.Add(item);
         var ci = _data.Count - 1; // child index; start at end

         while (ci > 0)
         {
            var pi = (ci - 1) / 2; // parent index

            if (_data[ci].Compare(_data[pi]) >= 0) 
               break; // child item is larger than (or equal) parent so we're done

            var tmp = _data[ci]; _data[ci] = _data[pi]; _data[pi] = tmp;
            ci = pi;
         }
      }

      public Cell Dequeue()
      {
         // assumes pq is not empty; up to calling code
         var li = _data.Count - 1; // last index (before removal)
         var frontItem = _data[0];   // fetch the front
         _data[0] = _data[li];
         _data.RemoveAt(li);

         --li; // last index (after removal)
         var pi = 0; // parent index. start at front of priority queue

         while (true)
         {
            var ci = pi * 2 + 1; // left child index of parent

            if (ci > li) 
               break;  // no children so done

            var rc = ci + 1;     // right child

            if (rc <= li && _data[rc].Compare(_data[ci]) < 0) // if there is a rc (ci + 1), and it is smaller than left child, use the rc instead
               ci = rc;

            if (_data[pi].Compare(_data[ci]) <= 0) 
               break; // parent is smaller than (or equal to) smallest child so done

            var tmp = _data[pi]; _data[pi] = _data[ci]; _data[ci] = tmp; // swap parent and child
            pi = ci;
         }

         return frontItem;
      }

      public bool Contains(Cell item)
      {
         return _data.Contains(item);
      }

      #endregion

   }
}
