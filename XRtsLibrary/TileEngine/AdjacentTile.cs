﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace XRtsLibrary.TileEngine
{
   public class AdjacentTile
   {

      #region Fields

      private static Dictionary<Tile, Point> _adjacentsTiles;

      #endregion

      #region Constructors

      public AdjacentTile()
      {
         _adjacentsTiles = new Dictionary<Tile, Point>();
      }

      #endregion

      #region Methods

      public void AddNeighbours(Point tile, MapLayer collisionsMap)
      {
         var adjacentTile = collisionsMap.GetTile(tile.X - 1, tile.Y + 1);
         _adjacentsTiles.Add(adjacentTile, adjacentTile.Tileset != 0 ? Point.Zero : new Point(tile.X - 1, tile.Y + 1));

         adjacentTile = collisionsMap.GetTile(tile.X, tile.Y + 1);
         _adjacentsTiles.Add(adjacentTile, adjacentTile.Tileset != 0 ? Point.Zero : new Point(tile.X, tile.Y + 1));

         adjacentTile = collisionsMap.GetTile(tile.X + 1, tile.Y + 1);
         _adjacentsTiles.Add(adjacentTile, adjacentTile.Tileset != 0 ? Point.Zero : new Point(tile.X + 1, tile.Y + 1));

         adjacentTile = collisionsMap.GetTile(tile.X - 1, tile.Y);
         _adjacentsTiles.Add(adjacentTile, adjacentTile.Tileset != 0 ? Point.Zero : new Point(tile.X - 1, tile.Y));

         adjacentTile = collisionsMap.GetTile(tile.X + 1, tile.Y);
         _adjacentsTiles.Add(adjacentTile, adjacentTile.Tileset != 0 ? Point.Zero : new Point(tile.X + 1, tile.Y));

         adjacentTile = collisionsMap.GetTile(tile.X - 1, tile.Y - 1);
         _adjacentsTiles.Add(adjacentTile, adjacentTile.Tileset != 0 ? Point.Zero : new Point(tile.X - 1, tile.Y - 1));

         adjacentTile = collisionsMap.GetTile(tile.X, tile.Y - 1);
         _adjacentsTiles.Add(adjacentTile, adjacentTile.Tileset != 0 ? Point.Zero : new Point(tile.X, tile.Y - 1));

         adjacentTile = collisionsMap.GetTile(tile.X + 1, tile.Y - 1);
         _adjacentsTiles.Add(adjacentTile, adjacentTile.Tileset != 0 ? Point.Zero : new Point(tile.X + 1, tile.Y - 1));
      }
      
      public Point GetFirstFreeTile()
      {
         var foundedTile = Point.Zero;

         foreach (var keyValuePair in _adjacentsTiles)
         {
            if (keyValuePair.Value != Point.Zero)
            {
               foundedTile = keyValuePair.Value;
               break;
            }
         }
         return foundedTile;
      }

      public bool IsExit()
      {
         var valueColl = _adjacentsTiles.Values;

         return valueColl.Any(point => point != Point.Zero);     //if any point is passable, then immediately return true
      }

      #endregion

   }
}
