﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using Microsoft.Xna.Framework;
using XRtsLibrary.Globals;

namespace XRtsLibrary.TileEngine
{
   public class Engine
   {

      #region Properties

      public static int TileWidth { get; private set; } = GameParameters.TileWidthPixels;

      public static int TileHeight { get; private set; } = GameParameters.TileHeightPixels;

      #endregion

      #region Constructors

      public Engine(int tileWidth, int tileHeight)
      {
         TileWidth = tileWidth;
         TileHeight = tileHeight;
      }

      #endregion

      #region Methods

      public static Point VectorToCell(Vector2 position)
      {
         return new Point((int)position.X / TileWidth, (int)position.Y / TileHeight);
      }

      public static Vector2 CellToVector(Point point)
      {
         var position = new Vector2(point.X * TileWidth, point.Y * TileHeight);

         if (point.X != 0)
            position.X++;

         if (point.Y != 0)
            position.Y++;

         return position;
      }

      #endregion

   }
}