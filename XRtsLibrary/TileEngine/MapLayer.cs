﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

namespace XRtsLibrary.TileEngine
{
    public class MapLayer
   {

      #region Fields

      private readonly Tile[,] _map;

      #endregion

      #region Properties

      public int Width => _map.GetLength(1);

      public int Height => _map.GetLength(0);

      #endregion

      #region Constructors

      public MapLayer(Tile[,] map)
      {
         _map = (Tile[,])map.Clone();
      }

      public MapLayer(int width, int height)
      {
         _map = new Tile[height, width];

         for (var y = 0; y < height; y++)
         {
            for (var x = 0; x < width; x++)
            {
               _map[y, x] = new Tile(0, 0);
            }
         }
      }

      #endregion

      #region Methods

      public Tile GetTile(int x, int y)
      {
         return _map[y, x];
      }

      public void SetTile(int x, int y, Tile tile)
      {
         _map[y, x] = tile;
      }

      public void SetTile(int x, int y, int tileIndex, int tileset)
      {
         _map[y, x] = new Tile(tileIndex, tileset);
      }

      #endregion

   }
}