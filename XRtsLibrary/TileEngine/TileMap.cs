﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XRtsLibrary.TileEngine
{
    public class TileMap
   {

      #region Fields

      private readonly List<Tileset> _tilesets;
      private readonly List<MapLayer> _mapLayers;

      private static int _mapWidth;
      private static int _mapHeight;

      #endregion

      #region Properties

      public static int WidthInPixels => _mapWidth * Engine.TileWidth;

      public static int HeightInPixels => _mapHeight * Engine.TileHeight;

      #endregion

      #region Constructors

      public TileMap(List<Tileset> tilesets, List<MapLayer> layers)
      {
         _tilesets = tilesets;
         _mapLayers = layers;

         _mapWidth = _mapLayers[0].Width;
         _mapHeight = _mapLayers[0].Height;

         for (var i = 1; i < layers.Count; i++)
         {
            if (_mapWidth != _mapLayers[i].Width || _mapHeight != _mapLayers[i].Height)
               throw new Exception("Map layer size exception");
         }
      }

      public TileMap(Tileset tileset, MapLayer layer)
      {
         _tilesets = new List<Tileset> {tileset};

         _mapLayers = new List<MapLayer> {layer};

         _mapWidth = _mapLayers[0].Width;
         _mapHeight = _mapLayers[0].Height;
      }

      #endregion

      #region Methods

      public void Draw(SpriteBatch spriteBatch, Camera camera)
      {
         var destination = new Rectangle(0, 0, Engine.TileWidth, Engine.TileHeight);

         foreach (var layer in _mapLayers)
         {
            for (var y = 0; y < layer.Height; y++)
            {
               destination.Y = y * Engine.TileHeight;

               for (var x = 0; x < layer.Width; x++)
               {
                  var tile = layer.GetTile(x, y);

                  if (tile.TileIndex == -1 || tile.Tileset == -1)
                     continue;

                  destination.X = x * Engine.TileWidth;

                  spriteBatch.Draw(
                     _tilesets[tile.Tileset].Texture,
                     destination,
                     _tilesets[tile.Tileset].SourceRectangles[tile.TileIndex],
                     Color.White
                  );
               }
            }
         }
      }

      public void AddLayer(MapLayer layer)
      {
         if (layer.Width != _mapWidth && layer.Height != _mapHeight)
            throw new Exception("Map layer size exception");

         _mapLayers.Add(layer);
      }

      #endregion

   }
}