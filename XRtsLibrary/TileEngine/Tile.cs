﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

namespace XRtsLibrary.TileEngine
{
    public class Tile
   {

      #region Fields

      private int _tileIndex;
      private int _tileset;

      #endregion

      #region Properties

      public int TileIndex
      {
         get { return _tileIndex; }
         private set { _tileIndex = value; }
      }

      public int Tileset
      {
         get { return _tileset; }
         private set { _tileset = value; }
      }

      #endregion

      #region Constructors

      public Tile(int tileIndex, int tileset)
      {
         TileIndex = tileIndex;
         Tileset = tileset;
      }

      #endregion

   }
}