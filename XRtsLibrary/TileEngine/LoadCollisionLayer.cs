﻿using System;
using System.Xml;

namespace XRtsLibrary.TileEngine
{
   public static class LoadCollisionLayer
   {

      #region Methods

      public static MapLayer LoadCollisionsLayer(string fileName)
      {
         var xmlDoc = new XmlDocument();

         try
         {
            xmlDoc.Load(fileName);
         }
         catch
         {
            throw new Exception("Unable to find the file.");
         }

         var rootNode = xmlDoc.FirstChild;

         if (rootNode.Name != "Layers")
         {
            throw new Exception("Invalid tile map format!");
         }

         var mapWidth = int.Parse(rootNode.Attributes["Width"].Value);
         var mapHeight = int.Parse(rootNode.Attributes["Height"].Value);
         var layer = new MapLayer(mapWidth, mapHeight);
         var random = new Random();
         var rowCount = 0;

         var layersNode = rootNode.FirstChild;

         if (layersNode.Name != "Layer")
         {
            throw new Exception("Invalid tile map format!");
         }

         foreach (XmlNode node in layersNode.ChildNodes)
         {
            try
            {
               if (node.Name == "Row")
               {
                  var row = node.InnerText;

                  row.Trim();
                  var cells = row.Split(' ');

                  for (var x = 0; x < mapWidth; x++)
                  {
                     if (cells[x] == "1")
                     {
                        var index = random.Next(2, 14);

                        var tile = new Tile(index, 1);
                        layer.SetTile(x, rowCount, tile);
                     }
                  }
                  rowCount++;
               }
            }
            catch
            {
               throw new Exception("Error reading in map layer!");
            }
         }
         return layer;
      }

      #endregion

   }
}