﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using XRtsLibrary.SpriteClasses;

namespace XRtsLibrary.TileEngine
{
   public enum CameraMode { Free, Follow }

   public class Camera
   {

      #region Fields

      private Vector2 _position;
      private float _speed;
      private float _zoom;
      private Rectangle _viewportRectangle;
      private CameraMode _mode;

      #endregion

      #region Properties

      public Vector2 Position
      {
         get { return _position; }
         private set { _position = value; }
      }

      public float Speed
      {
         get { return _speed; }
         set
         {
            _speed = MathHelper.Clamp(_speed, 1f, 16f);
         }
      }

      public float Zoom => _zoom;

      public CameraMode CameraMode => _mode;

      public Matrix Transformation => Matrix.CreateScale(_zoom) *
                                      Matrix.CreateTranslation(new Vector3(-Position, 0f));

      public Rectangle ViewportRectangle => new Rectangle(
         _viewportRectangle.X,
         _viewportRectangle.Y,
         _viewportRectangle.Width,
         _viewportRectangle.Height
      );

      #endregion

      #region Constructors

      public Camera(Rectangle viewportRect)
      {
         _speed = 4f;
         _zoom = 1f;
         _viewportRectangle = viewportRect;
         _mode = CameraMode.Free;
      }

      public Camera(Rectangle viewportRect, Vector2 position)
      {
         _speed = 4f;
         _zoom = 1f;
         _viewportRectangle = viewportRect;
         Position = position;
         _mode = CameraMode.Free;
      }

      #endregion

      #region Methods

      public void Update(GameTime gameTime)
      {
         if (_mode == CameraMode.Follow)
            return;

         var motion = Vector2.Zero;

         if (InputHandler.KeyDown(Keys.Left) ||
            InputHandler.ButtonDown(Buttons.RightThumbstickLeft, PlayerIndex.One))
            motion.X = -_speed;
         else if (InputHandler.KeyDown(Keys.Right) ||
            InputHandler.ButtonDown(Buttons.RightThumbstickRight, PlayerIndex.One))
            motion.X = _speed;

         if (InputHandler.KeyDown(Keys.Up) ||
            InputHandler.ButtonDown(Buttons.RightThumbstickUp, PlayerIndex.One))
            motion.Y = -_speed;
         else if (InputHandler.KeyDown(Keys.Down) ||
            InputHandler.ButtonDown(Buttons.RightThumbstickDown, PlayerIndex.One))
            motion.Y = _speed;

         if (motion != Vector2.Zero)
         {
            motion.Normalize();
            _position += motion * _speed;
            LockCamera();
         }
      }

      public void ZoomIn()
      {
         _zoom += .25f;

         if (_zoom > 2.5f)
            _zoom = 2.5f;

         var newPosition = Position * _zoom;
         SnapToPosition(newPosition);
      }

      public void ZoomOut()
      {
         _zoom -= .25f;

         if (_zoom < .5f)
            _zoom = .5f;

         var newPosition = Position * _zoom;
         SnapToPosition(newPosition);
      }

      private void SnapToPosition(Vector2 newPosition)
      {
         _position.X = newPosition.X - _viewportRectangle.Width / 2;
         _position.Y = newPosition.Y - _viewportRectangle.Height / 2;
         LockCamera();
      }

      private void LockCamera()
      {
         _position.X = MathHelper.Clamp(
            _position.X,
            0,
            TileMap.WidthInPixels * _zoom - _viewportRectangle.Width
         );

         _position.Y = MathHelper.Clamp(
            _position.Y,
            0,
            TileMap.HeightInPixels * _zoom - _viewportRectangle.Height
         );
      }

      public void LockToSprite(AnimatedSprite sprite)
      {
         _position.X = (sprite.Position.X + sprite.Width / 2) * _zoom
            - (_viewportRectangle.Width / 2);
         _position.Y = (sprite.Position.Y + sprite.Height / 2) * _zoom
            - (_viewportRectangle.Height / 2);
         LockCamera();
      }

      public void ToggleCameraMode()
      {
         switch (_mode)
         {
            case CameraMode.Follow:
               _mode = CameraMode.Free;
               break;
            case CameraMode.Free:
               _mode = CameraMode.Follow;
               break;
            default:
            {
               throw new ArgumentOutOfRangeException();
            }
         }
      }

      #endregion

   }
}