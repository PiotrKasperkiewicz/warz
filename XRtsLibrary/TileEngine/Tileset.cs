﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XRtsLibrary.TileEngine
{
    public class Tileset
   {

      #region Fields

      private Texture2D _image;
      private int _tileWidthInPixels;
      private int _tileHeightInPixels;
      private int _tilesWide;
      private int _tilesHigh;
      private readonly Rectangle[] _sourceRectangles;

      #endregion

      #region Properties

      public Texture2D Texture
      {
         get { return _image; }
         private set { _image = value; }
      }

      public int TileWidth
      {
         get { return _tileWidthInPixels; }
         private set { _tileWidthInPixels = value; }
      }

      public int TileHeight
      {
         get { return _tileHeightInPixels; }
         private set { _tileHeightInPixels = value; }
      }

      public int TilesWide
      {
         get { return _tilesWide; }
         private set { _tilesWide = value; }
      }

      public int TilesHigh
      {
         get { return _tilesHigh; }
         private set { _tilesHigh = value; }
      }

      public Rectangle[] SourceRectangles => (Rectangle[])_sourceRectangles.Clone();

      #endregion

      #region Constructors

      public Tileset(Texture2D image, int tilesWide, int tilesHigh, int tileWidth, int tileHeight)
      {
         Texture = image;
         TileWidth = tileWidth;
         TileHeight = tileHeight;
         TilesWide = tilesWide;
         TilesHigh = tilesHigh;

         var tiles = tilesWide * tilesHigh;

         _sourceRectangles = new Rectangle[tiles];

         var tile = 0;

         for (var y = 0; y < tilesHigh; y++)
            for (var x = 0; x < tilesWide; x++)
            {
               _sourceRectangles[tile] = new Rectangle(
                  x * tileWidth,
                  y * tileHeight,
                  tileWidth,
                  tileHeight
               );
               tile++;
            }
      }

      #endregion

   }
}