/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace XRtsLibrary
{
    public class InputHandler : GameComponent
   {

      #region Keyboard Fields

      static KeyboardState keyboardState;
      static KeyboardState lastKeyboardState;

      #endregion

      #region Game Pad Fields

      static GamePadState[] gamePadStates;
      static GamePadState[] lastGamePadStates;

      #endregion
      //added
      #region Mouse Fields

      static MouseState mouseState;
      static MouseState lastMouseState;

      static Point lastMousePosition;
      static Point mousePosition;

      #endregion

      #region Keyboard Properties

      public static KeyboardState KeyboardState
      {
         get { return keyboardState; }
      }

      public static KeyboardState LastKeyboardState
      {
         get { return lastKeyboardState; }
      }

      #endregion

      #region Game Pad Properties

      public static GamePadState[] GamePadStates
      {
         get { return gamePadStates; }
      }

      public static GamePadState[] LastGamePadStates
      {
         get { return lastGamePadStates; }
      }

      #endregion
      //added
      #region Mouse Properties

      public static MouseState MouseState
      {
         get { return mouseState; }
      }

      public static MouseState LastMouseState
      {
         get { return lastMouseState; }
      }

      public static Point LastMousePosition
      {
         get { return lastMousePosition; }
      }

      public static Point MousePosition
      {
         get { return mousePosition; }
      }

      public static Vector2 CursorPosition
      {
         get { return new Vector2(mouseState.X, mouseState.Y); }
      }

      #endregion

      #region Constructors

      public InputHandler(Game game)
         : base(game)
      {
         keyboardState = Keyboard.GetState();
         mouseState = Mouse.GetState();   //added

         gamePadStates = new GamePadState[Enum.GetValues(typeof(PlayerIndex)).Length];

         foreach (PlayerIndex index in Enum.GetValues(typeof(PlayerIndex)))
            gamePadStates[(int)index] = GamePad.GetState(index);
      }

      #endregion

      #region XNA Methods

      public override void Initialize()
      {
         base.Initialize();
      }

      public override void Update(GameTime gameTime)
      {
         lastKeyboardState = keyboardState;
         keyboardState = Keyboard.GetState();
         //added mouse
         lastMouseState = mouseState;
         mouseState = Mouse.GetState();

         lastMousePosition.X = mousePosition.X;
         lastMousePosition.Y = mousePosition.Y;

         mousePosition.X = mouseState.X;
         mousePosition.Y = mouseState.Y;

         lastGamePadStates = (GamePadState[])gamePadStates.Clone();

         foreach (PlayerIndex index in Enum.GetValues(typeof(PlayerIndex)))
            gamePadStates[(int)index] = GamePad.GetState(index);

         base.Update(gameTime);
      }

      #endregion

      #region General Methods

      public static void Flush()
      {
         lastKeyboardState = keyboardState;
         lastMouseState = mouseState;
      }

      #endregion

      #region Keyboard

      public static bool KeyReleased(Keys key)
      {
         return keyboardState.IsKeyUp(key) &&
            lastKeyboardState.IsKeyDown(key);
      }

      public static bool KeyPressed(Keys key)
      {
         return keyboardState.IsKeyDown(key) &&
            lastKeyboardState.IsKeyUp(key);
      }

      public static bool KeyDown(Keys key)
      {
         return keyboardState.IsKeyDown(key);
      }

      #endregion

      #region Game Pad

      public static bool ButtonReleased(Buttons button, PlayerIndex index)
      {
         return gamePadStates[(int)index].IsButtonUp(button) &&
            lastGamePadStates[(int)index].IsButtonDown(button);
      }

      public static bool ButtonPressed(Buttons button, PlayerIndex index)
      {
         return gamePadStates[(int)index].IsButtonDown(button) &&
            lastGamePadStates[(int)index].IsButtonUp(button);
      }

      public static bool ButtonDown(Buttons button, PlayerIndex index)
      {
         return gamePadStates[(int)index].IsButtonDown(button);
      }

      #endregion
      //added
      #region Mouse
      
      public static bool LeftMouseReleased()
      {
         mouseState = Mouse.GetState();
         return mouseState.LeftButton == ButtonState.Released && lastMouseState.LeftButton == ButtonState.Pressed;
      }

      public static bool LeftMousePressed()
      {
         mouseState = Mouse.GetState();
         return mouseState.LeftButton == ButtonState.Pressed && lastMouseState.LeftButton == ButtonState.Released;
      }

      public static bool LeftMouseDown()
      {
         mouseState = Mouse.GetState();
         return mouseState.LeftButton == ButtonState.Pressed;
      }

      public static bool RightMouseReleased()
      {
         mouseState = Mouse.GetState();
         return mouseState.RightButton == ButtonState.Released && lastMouseState.RightButton == ButtonState.Pressed;
      }

      public static bool RightMousePressed()
      {
         mouseState = Mouse.GetState();
         return mouseState.RightButton == ButtonState.Pressed && lastMouseState.RightButton == ButtonState.Released;
      }

      public static bool RightMouseDown()
      {
         mouseState = Mouse.GetState();
         return mouseState.RightButton == ButtonState.Pressed;
      }

      public static bool MouseoverOnArea(Rectangle area)
      {
         return (!area.Contains(lastMousePosition) && area.Contains(mousePosition));
      }

      public static bool MouseoverFromArea(Rectangle area)
      {
         return (area.Contains(lastMousePosition) && !area.Contains(mousePosition));
      }

      public static bool MouseClickedOnArea(Rectangle area)
      {
         return (LeftMouseReleased() && area.Contains(MousePosition));
      }

      public static bool MouseClickedOnAreaR(Rectangle area)
      {
         return (RightMouseReleased() && area.Contains(MousePosition));
      }

      public static bool Mouseover(Rectangle area)
      {
         return area.Contains(MousePosition);
      }

      public static bool Mouseover(Rectangle area1, Rectangle area2)
      {
         return area1.Contains(MousePosition) || area2.Contains(MousePosition);
      }

      public static bool Mouseover(Rectangle area1, Rectangle area2, Rectangle area3)
      {
         return area1.Contains(MousePosition) || area2.Contains(MousePosition) || area3.Contains(MousePosition);
      }

      //added scroll wheel property
      public static int ScrollValue()
      {
         mouseState = Mouse.GetState();
         return mouseState.ScrollWheelValue;
      }

      #endregion

   }
}