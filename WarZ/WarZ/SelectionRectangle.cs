﻿/* Original author: "Mille Boström"
 * Sources: 
 * https://github.com/Meeii/SpeedCodingWarcraft2
 * https://www.youtube.com/watch?v=YWG72psq8qI
 * Original title: "How to make Warcraft 2 - Part 1: Mouse and map objects (C#. XNA, Monogame)"
 */

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace WarZ
{
   internal static class SelectionRectangle
   {

      #region Fields

      static Texture2D frameTexture;

      #endregion

      #region XNA Methods

      public static void LoadContent(ContentManager Content)
      {
         frameTexture = Content.Load<Texture2D>(@"Cursors\selectionUnits");
      }

      public static void Draw(SpriteBatch spriteBatch, Rectangle rectangle)
      {
         if (frameTexture == null)
            return;

         spriteBatch.Draw(frameTexture, new Rectangle(rectangle.X, rectangle.Y, rectangle.Width, 2), Color.White);
         spriteBatch.Draw(frameTexture, new Rectangle(rectangle.X + rectangle.Width - 2, rectangle.Y, 2, rectangle.Height), Color.White);
         spriteBatch.Draw(frameTexture, new Rectangle(rectangle.X, rectangle.Y + rectangle.Height - 2, rectangle.Width, 2), Color.White);
         spriteBatch.Draw(frameTexture, new Rectangle(rectangle.X, rectangle.Y, 2, rectangle.Height), Color.White);
      }

      #endregion

   }
}