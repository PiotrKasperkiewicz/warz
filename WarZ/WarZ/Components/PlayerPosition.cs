﻿/*
 * Copyright (c) 2013 Tomasz Hachaj
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace WarZ.Components
{
   [Serializable]
   public class PlayerPosition         //this class is responsible for sending player position and other informations to rest of players
   {

      #region Fields

      public Vector2 Position;

      #endregion

      #region Methods

      private static void ConvertAndAddToArray(IList<byte> data, float number, ref int actualPosition, bool reverse)
      {
         var helpArray = BitConverter.GetBytes(number);

         if (reverse)
            Array.Reverse(helpArray);

         for (var a = 0; a < helpArray.Length; a++)
            data[actualPosition + a] = helpArray[a];

         actualPosition += helpArray.Length;
      }

      private static void ConvertAndAddToArray(IList<byte> data, int number, ref int actualPosition, bool reverse)
      {
         var helpArray = BitConverter.GetBytes(number);

         if (reverse)
            Array.Reverse(helpArray);

         for (var a = 0; a < helpArray.Length; a++)
            data[actualPosition + a] = helpArray[a];

         actualPosition += helpArray.Length;
      }

      private static void ConvertAndAddToArray(IList<byte> data, byte number, ref int actualPosition, bool reverse)
      {
         data[actualPosition] = number;
         actualPosition++;
      }

      public static byte[] ToByteArray(PlayerPosition data)
      {
         var outputData = new byte[(2*sizeof(float))];

         var actualPosition = 0;                            //current position of read pointer in byte array
         var reverse = BitConverter.IsLittleEndian;
         //only position for now...
         ConvertAndAddToArray(outputData, data.Position.X, ref actualPosition, reverse);
         ConvertAndAddToArray(outputData, data.Position.Y, ref actualPosition, reverse);                                                                                               

         return outputData;
      }

      private static float ConvertFloatFromArray(IList<byte> data, ref int actualPosition, bool reverse)
      {
         var helpArray = new byte[sizeof(float)];

         for (var a = 0; a < sizeof(float); a++)
            helpArray[a] = data[actualPosition + a];

         if (reverse)
            Array.Reverse(helpArray);

         actualPosition += helpArray.Length;

         return BitConverter.ToSingle(helpArray, 0);
      }

      private static int ConvertIntFromArray(IList<byte> data, ref int actualPosition, bool reverse)
      {
         var helpArray = new byte[sizeof(int)];

         for (var a = 0; a < sizeof(int); a++)
            helpArray[a] = data[actualPosition + a];

         if (reverse)
            Array.Reverse(helpArray);

         actualPosition += helpArray.Length;

         return BitConverter.ToInt32(helpArray, 0);
      }

      private static byte ConvertByteFromArray(IList<byte> data, ref int actualPosition, bool reverse)
      {
         var returnValue = data[actualPosition];

         actualPosition++;

         return returnValue;
      }

      public static PlayerPosition FromByteArray(byte[] data)
      {
         var playerPosition = new PlayerPosition();
         var actualPosition = 0;
         var reverse = BitConverter.IsLittleEndian;

         playerPosition.Position.X = ConvertFloatFromArray(data, ref actualPosition, reverse);
         playerPosition.Position.Y = ConvertFloatFromArray(data, ref actualPosition, reverse);

         return playerPosition;
      }

      #endregion

   }
}