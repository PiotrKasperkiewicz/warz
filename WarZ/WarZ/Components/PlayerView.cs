﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRtsLibrary.TileEngine;

namespace WarZ.Components
{
    public class PlayerView
   {

      #region Properties

      public Camera Camera { get; }

      #endregion

      #region Constructors

      public PlayerView(Game game)
      {
         var gameRef = (Game1)game;
         Camera = new Camera(gameRef.ScreenRectangle);
      }

      #endregion

      #region Methods

      public void Update(GameTime gameTime)
      {
         Camera.Update(gameTime);
      }

      public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
      {
      }

      #endregion

   }
}