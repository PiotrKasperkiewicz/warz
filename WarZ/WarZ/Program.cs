/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

namespace WarZ
{

   #if WINDOWS || XBOX || LINUX //check, if it could run on 2nd and 3rd platform

      internal static class Program
      {

         #region Methods

         private static void Main(string[] args)
         {
            using (var game = new Game1())
            {
               game.Run();
            }

         }

         #endregion

      }

   #endif

}