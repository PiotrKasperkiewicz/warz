/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XRtsLibrary;
using WarZ.GameScreens;

namespace WarZ
{
   public class Game1 : Game
   {

      #region XNA Fields

      public SpriteBatch SpriteBatch;
      public readonly OptionScreen OptionScreen;

      #endregion

      #region Game States

      private readonly GameStateManager _stateManager;
      public readonly StartMenuScreen StartMenuScreen;
      public readonly GamePlayScreen GamePlayScreen;

      #endregion

      #region Screen Fields

      private const int ScreenWidth = 1024;
      private const int ScreenHeight = 768;

      public readonly Rectangle ScreenRectangle;

      private Vector2 _cursorPosition;

      #endregion

      #region Properties

      private Texture2D CursorTexture { get; set; }

      #endregion

      #region Constructors

      public Game1()
      {
         var graphics = new GraphicsDeviceManager(this)
         {
            PreferredBackBufferWidth = ScreenWidth,
            PreferredBackBufferHeight = ScreenHeight
         };

         //SoundEffect.MasterVolume = GameParameters.masterVolume;

         ScreenRectangle = new Rectangle(
            0,
            0,
            ScreenWidth,
            ScreenHeight
         );

         Content.RootDirectory = "Content";

         Components.Add(new InputHandler(this));

         _stateManager = new GameStateManager(this);
         Components.Add(_stateManager);

         var titleScreen = new TitleScreen(this, _stateManager);
         StartMenuScreen = new StartMenuScreen(this, _stateManager);
         GamePlayScreen = new GamePlayScreen(this, _stateManager);
         OptionScreen = new OptionScreen(this, _stateManager);

         _stateManager.ChangeState(titleScreen);
      }

      #endregion

      #region XNA Methods

      /*                                                                 //unnecessary
      protected override void Initialize()
      {
         base.Initialize();
      }
      */

      protected override void LoadContent()
      {
         SpriteBatch = new SpriteBatch(GraphicsDevice);
         CursorTexture = Content.Load<Texture2D>(@"Cursors\cursor");
      }

      protected override void UnloadContent()
      {
      }

      protected override void Update(GameTime gameTime)
      {
         if (!IsActive)
            return;

         if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || InputHandler.KeyDown(Keys.Escape))
            _stateManager.ChangeState(StartMenuScreen);

         _cursorPosition = InputHandler.CursorPosition;

         base.Update(gameTime);
      }

      protected override void Draw(GameTime gameTime)
      {
         GraphicsDevice.Clear(Color.CornflowerBlue);

         base.Draw(gameTime);

         SpriteBatch.Begin();

         SpriteBatch.Draw(CursorTexture, _cursorPosition, Color.White);

         SpriteBatch.End();
      }

      #endregion

   }
}