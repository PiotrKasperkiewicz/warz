﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRtsLibrary;
using XRtsLibrary.Controls;

namespace WarZ.GameScreens
{
    public class StartMenuScreen : BaseGameState
   {

      #region Fields

      private PictureBox _backgroundImage;
      private PictureBox _arrowImage;
      private LinkLabel _multiplayerGame;
      private LinkLabel _loadGame;
      private LinkLabel _exitGame;
      private float _maxItemWidth = 0f;

      #endregion

      #region Constructors

      public StartMenuScreen(Game game, GameStateManager manager)
         : base(game, manager)
      {
      }

      #endregion

      #region XNA Methods
      /*
      public override void Initialize()
      {
         base.Initialize();
      }*/

      protected override void LoadContent()
      {
         base.LoadContent();

         var content = Game.Content;

         _backgroundImage = new PictureBox(
            content.Load<Texture2D>(@"Backgrounds\startscreen2"),
            GameRef.ScreenRectangle
         );
         ControlManager.Add(_backgroundImage);

         var arrowTexture = content.Load<Texture2D>(@"GUI\leftarrowUp");

         _arrowImage = new PictureBox(
            arrowTexture,
            new Rectangle(
               0,
               0,
               arrowTexture.Width,
               arrowTexture.Height
            )
         );
         ControlManager.Add(_arrowImage);

         _multiplayerGame = new LinkLabel {Text = "New game"};
         _multiplayerGame.Size = _multiplayerGame.SpriteFont.MeasureString(_multiplayerGame.Text);
         _multiplayerGame.Selected += menuItem_Selected;

         ControlManager.Add(_multiplayerGame);

         _loadGame = new LinkLabel {Text = "Load Game"};
         _loadGame.Size = _loadGame.SpriteFont.MeasureString(_loadGame.Text);
         _loadGame.Selected += menuItem_Selected;

         ControlManager.Add(_loadGame);

         _exitGame = new LinkLabel {Text = "Exit"};
         _exitGame.Size = _exitGame.SpriteFont.MeasureString(_exitGame.Text);
         _exitGame.Selected += menuItem_Selected;

         ControlManager.Add(_exitGame);

         ControlManager.NextControl();

         ControlManager.FocusChanged += ControlManager_FocusChanged;
         var position = new Vector2(350, 500);

         foreach (var control in ControlManager)
         {
            if (control is LinkLabel)
            {
               if (control.Size.X > _maxItemWidth)
                  _maxItemWidth = control.Size.X;

               control.Position = position;
               position.Y += control.Size.Y + 5f;
            }
         }

         ControlManager_FocusChanged(_multiplayerGame, null);
         ControlManager.MeasureControls();
      }

      private void ControlManager_FocusChanged(object sender, EventArgs e)
      {
         var control = sender as Control;

         var position = new Vector2(
            control.Position.X + _maxItemWidth + 10f,
            control.Position.Y
         );
         _arrowImage.SetPosition(position);
      }

      private void menuItem_Selected(object sender, EventArgs e)
      {
         if (sender == _multiplayerGame)
         {
            StateManager.PushState(GameRef.OptionScreen);
         }

         if (sender == _loadGame)
         {
            StateManager.PushState(GameRef.GamePlayScreen);
         }

         if (sender == _exitGame)
         {
            GameRef.Exit();
         }
      }

      public override void Update(GameTime gameTime)
      {
         ControlManager.Update(gameTime, playerIndexInControl);

         base.Update(gameTime);
      }

      public override void Draw(GameTime gameTime)
      {
         GameRef.SpriteBatch.Begin();

         base.Draw(gameTime);

         ControlManager.Draw(GameRef.SpriteBatch);

         GameRef.SpriteBatch.End();
      }

      #endregion

   }
}