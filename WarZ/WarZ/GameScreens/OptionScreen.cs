﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRtsLibrary;
using XRtsLibrary.Controls;

namespace WarZ.GameScreens
{
    public class OptionScreen : BaseGameState
   {

      #region Fields

      private LeftRightSelector _optionSelector1;
      private LeftRightSelector _optionSelector2;
      private PictureBox _backgroundImage;

      private readonly string[] _options1 = { "Option 1", "Option 2" };
      private readonly string[] _options2 = { "Option 1", "Option 2", "Option 3", "Option 4" };

      #endregion

      #region Constructors

      public OptionScreen(Game game, GameStateManager stateManager)
         : base(game, stateManager)
      {
      }

      #endregion

      #region XNA Methods
      /*
      public override void Initialize()
      {
         base.Initialize();
      }*/

      protected override void LoadContent()
      {
         base.LoadContent();

         CreateControls();
      }

      public override void Update(GameTime gameTime)
      {
         ControlManager.Update(gameTime, PlayerIndex.One);

         base.Update(gameTime);
      }

      public override void Draw(GameTime gameTime)
      {
         GameRef.SpriteBatch.Begin();

         base.Draw(gameTime);

         ControlManager.Draw(GameRef.SpriteBatch);
         
         GameRef.SpriteBatch.End();
      }

      #endregion

      #region Methods

      private void CreateControls()
      {
         var leftTexture = Game.Content.Load<Texture2D>(@"GUI\leftarrowUp");
         var rightTexture = Game.Content.Load<Texture2D>(@"GUI\rightarrowUp");
         var stopTexture = Game.Content.Load<Texture2D>(@"GUI\StopBar");

         _backgroundImage = new PictureBox(
            Game.Content.Load<Texture2D>(@"Backgrounds\startscreen2"),
            GameRef.ScreenRectangle
         );

         ControlManager.Add(_backgroundImage);

         var label1 = new Label
         {
            Text = "Customize Your settings"
         };

         label1.Size = label1.SpriteFont.MeasureString(label1.Text);
         label1.Position = new Vector2((GameRef.Window.ClientBounds.Width - label1.Size.X) / 2, 450);

         ControlManager.Add(label1);

         _optionSelector1 = new LeftRightSelector(leftTexture, rightTexture, stopTexture);
         //OptionSelector1.SetItems(genderItems, 125);    modified to below:
         _optionSelector1.SetItems(_options1);
         _optionSelector1.Position = new Vector2(label1.Position.X, 500);

         ControlManager.Add(_optionSelector1);

         _optionSelector2 = new LeftRightSelector(leftTexture, rightTexture, stopTexture);
         //OptionSelector2.SetItems(classItems, 125);       modified to below:
         _optionSelector2.SetItems(_options2);
         _optionSelector2.Position = new Vector2(label1.Position.X, 550);

         ControlManager.Add(_optionSelector2);

         var linkLabel1 = new LinkLabel
         {
            Text = "Apply settings",
            Position = new Vector2(label1.Position.X, 600)
            //Selected += new EventHandler(linkLabel1_Selected)
         };

         linkLabel1.Size = linkLabel1.SpriteFont.MeasureString(linkLabel1.Text);
         linkLabel1.Selected += linkLabel1_Selected;

         ControlManager.Add(linkLabel1);

         ControlManager.NextControl();

         ControlManager.MeasureControls();
      }

      private void linkLabel1_Selected(object sender, EventArgs e)
      {
         InputHandler.Flush();

         StateManager.PopState();
         StateManager.PushState(GameRef.GamePlayScreen);
      }

      #endregion

   }
}