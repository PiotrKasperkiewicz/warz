﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */


using XRtsLibrary;
using XRtsLibrary.Controls;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace WarZ.GameScreens
{
    public abstract partial class BaseGameState : GameState
   {
      
      #region Fields

      protected Game1 GameRef;

      protected ControlManager ControlManager;

      protected PlayerIndex playerIndexInControl;

      #endregion

      #region Properties

      #endregion

      #region Constructors

      public BaseGameState(Game game, GameStateManager manager)
         : base(game, manager)
      {
         GameRef = (Game1)game;

         playerIndexInControl = PlayerIndex.One;
      }

      #endregion

      #region XNA Methods

      protected override void LoadContent()
      {
         ContentManager Content = Game.Content;

         SpriteFont menuFont = Content.Load<SpriteFont>(@"Fonts\ControlFont");
         ControlManager = new ControlManager(menuFont);

         base.LoadContent();
      }

      public override void Update(GameTime gameTime)
      {
         base.Update(gameTime);
      }

      public override void Draw(GameTime gameTime)
      {
         base.Draw(gameTime);
      }

      #endregion

   }
}