﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 * 
 * Copyright (c) 2013 Tomasz Hachaj
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XRtsLibrary;
using XRtsLibrary.TileEngine;
using XRtsLibrary.SpriteClasses;
using XRtsLibrary.Globals;
using WarZ.Components;
using WarZ.Multiplayer;

namespace WarZ.GameScreens
{
   public class GamePlayScreen : BaseGameState
   {

      #region Fields

      private readonly int _tileWidth = GameParameters.TileWidthPixels;
      private readonly int _tileHeight = GameParameters.TileHeightPixels;
      private readonly int _spriteWidth = GameParameters.SpriteWidthPixels;
      private readonly int _spriteHeight = GameParameters.SpriteHeightPixels;
      private readonly int _mapWidth = GameParameters.MapWidth;
      private readonly int _mapHeight = GameParameters.MapHeight;

      //private readonly Point[] _playerOneStartPoints = GameParameters.PlayerOneStart;
      private Point _playerOneStart = GameParameters.PlayerOneStart;

      private readonly int _port = GameParameters.Port;
      private readonly string _addressIp = GameParameters.AddressIP;
      private TCPClient _client;

      private TileMap _map;
      private MapLayer _copySplatter;

      private readonly PlayerView _playerView;

      private AnimatedSprite _sprite;

      private bool _selected;
      private Vector2 _selectedCorner;
      private Rectangle _selectRectangle;
      private Vector2 _mousePosition;

      //added for mouse scroll wheel
      private int _previousScrollValue;                 

      private static readonly PlayerPosition SecondPlayerPosition = new PlayerPosition();

      #endregion

      #region Constructors

      public GamePlayScreen(Game game, GameStateManager manager)
         : base(game, manager)
      {
         _playerView = new PlayerView(game);
      }

      #endregion

      #region XNA Methods

      public override void Initialize()
      {
         //added Scroll original value when game was started:
         _previousScrollValue = InputHandler.ScrollValue();
         _selectRectangle = new Rectangle(0, 0, 0, 0);

         _client = new TCPClient(_addressIp, _port);
         _client.StartConnection();
         _client.NewData += NewDataRecived;

         base.Initialize();
      }

      protected override void LoadContent()
      {
         var spriteSheet = Game.Content.Load<Texture2D>(@"PlayerSprites\KnightSpriteFull");   //modified, previous was 'KnightSpriteImproved'

         var animations = new Dictionary<AnimationKey, Animation>();

         var animation = new Animation(3, _spriteWidth, _spriteHeight, 0, 0);
         animations.Add(AnimationKey.Down, animation);

         animation = new Animation(3, _spriteWidth, _spriteHeight, 0, 32);
         animations.Add(AnimationKey.Left, animation);

         animation = new Animation(3, _spriteWidth, _spriteHeight, 0, 64);
         animations.Add(AnimationKey.Right, animation);

         animation = new Animation(3, _spriteWidth, _spriteHeight, 0, 96);
         animations.Add(AnimationKey.Up, animation);

         //added diagonal animations:
         animation = new Animation(3, _spriteWidth, _spriteHeight, 0, 128);
         animations.Add(AnimationKey.RU, animation);

         animation = new Animation(3, _spriteWidth, _spriteHeight, 0, 160);
         animations.Add(AnimationKey.LU, animation);

         animation = new Animation(3, _spriteWidth, _spriteHeight, 0, 192);
         animations.Add(AnimationKey.RD, animation);

         animation = new Animation(3, _spriteWidth, _spriteHeight, 0, 224);
         animations.Add(AnimationKey.LD, animation);

         _sprite = new AnimatedSprite(spriteSheet, animations);

         base.LoadContent();

         var tilesetTexture = Game.Content.Load<Texture2D>(@"Tilesets\tileset1");
         var tileset1 = new Tileset(tilesetTexture, 8, 8, _tileWidth, _tileHeight);

         tilesetTexture = Game.Content.Load<Texture2D>(@"Tilesets\tileset2");
         var tileset2 = new Tileset(tilesetTexture, 8, 8, _tileWidth, _tileHeight);

         tilesetTexture = Game.Content.Load<Texture2D>(@"Tilesets\tileset3");
         var tileset3 = new Tileset(tilesetTexture, 8, 8, _tileWidth, _tileHeight);

         var tilesets = new List<Tileset> { tileset1, tileset2, tileset3 };

         var layer = new MapLayer(_mapWidth, _mapHeight);                                                      

         for (var y = 0; y < layer.Height; y++)
         {
            for (var x = 0; x < layer.Width; x++)
            {
               var tile = new Tile(0, 0);

               layer.SetTile(x, y, tile);
            }
         }

         var splatter = LoadCollisionLayer.LoadCollisionsLayer(@"Content\CollisionsLayer\collisions.tmap");

         splatter.SetTile(1, 0, new Tile(0, 2));         //adding castle : walls and gate
         splatter.SetTile(2, 0, new Tile(2, 2));
         splatter.SetTile(3, 0, new Tile(0, 2));

         var mapLayers = new List<MapLayer> {layer, splatter};

         _map = new TileMap(tilesets, mapLayers);

         _copySplatter = splatter;

         SelectionRectangle.LoadContent(GameRef.Content);

         _sprite.Position = Engine.CellToVector(StartpointCollisionHandler.CheckStartpoint(_playerOneStart, splatter));
      }

      protected override void UnloadContent()
      {
         _client.StopConnection();
      }

      public override void Update(GameTime gameTime)
      {
         _playerView.Update(gameTime);
         _sprite.Update(gameTime);

         SendDataToServer();

         if (InputHandler.LeftMousePressed())         //player just perform single click
         {
            _mousePosition = new Vector2(InputHandler.MousePosition.X, InputHandler.MousePosition.Y);
            _selectedCorner = _mousePosition;
            _selectRectangle = new Rectangle((int)_mousePosition.X, (int)_mousePosition.Y, 0, 0);
         }
         else if(InputHandler.LeftMouseDown())        //player perform click and hold left mouse button
         {
            _selectedCorner = new Vector2(InputHandler.MousePosition.X, InputHandler.MousePosition.Y);

            if (_selectedCorner.X > _mousePosition.X)
            {
               _selectRectangle.X = (int)_mousePosition.X;
            }
            else
            {
               _selectRectangle.X = (int)_selectedCorner.X;
            }

            if (_selectedCorner.Y > _mousePosition.Y)
            {
               _selectRectangle.Y = (int)_mousePosition.Y;
            }
            else
            {
               _selectRectangle.Y = (int)_selectedCorner.Y;
            }

            _selectRectangle.Width = (int)Math.Abs(_mousePosition.X - _selectedCorner.X);
            _selectRectangle.Height = (int)Math.Abs(_mousePosition.Y - _selectedCorner.Y);
         }

         if(InputHandler.LeftMouseReleased())      //If player released left button, then reset selection rectangle
         {
            if (_selectRectangle.Intersects(_sprite.UnitRectangle))
            {
               _selected = true;
            }
            else
            {
               _selected = false;
            }

            _selectRectangle = new Rectangle(0, 0, 0, 0);
         }

         //added functionality for get clicked tile on map
         //modified: right click moves sprite to clicked tile
         //if(InputHandler.RightMouseDown())   //modified: it caused many outputs with coordinates to debug instead one per one click
         if (InputHandler.RightMousePressed())
            if (_selected)
               _sprite.Move(_copySplatter);

         if (InputHandler.KeyReleased(Keys.PageUp) ||
            InputHandler.ButtonReleased(Buttons.LeftShoulder, PlayerIndex.One) || _previousScrollValue < InputHandler.ScrollValue())
         {
            _playerView.Camera.ZoomIn();

            if (_playerView.Camera.CameraMode == CameraMode.Follow)
               _playerView.Camera.LockToSprite(_sprite);
         }
         else if (InputHandler.KeyReleased(Keys.PageDown) ||
            InputHandler.ButtonReleased(Buttons.RightShoulder, PlayerIndex.One) || _previousScrollValue > InputHandler.ScrollValue())
         {
            _playerView.Camera.ZoomOut();

            if (_playerView.Camera.CameraMode == CameraMode.Follow)
               _playerView.Camera.LockToSprite(_sprite);
         }

         //added cause of Zoom feature and Scroll Wheel
         _previousScrollValue = InputHandler.ScrollValue();
         
         var motion = new Vector2();
         
         if (InputHandler.KeyDown(Keys.W) ||
            InputHandler.ButtonDown(Buttons.LeftThumbstickUp, PlayerIndex.One))
         {
            _sprite.CurrentAnimation = AnimationKey.Up;
            motion.Y = -1;
         }
         else if (InputHandler.KeyDown(Keys.S) ||
            InputHandler.ButtonDown(Buttons.LeftThumbstickDown, PlayerIndex.One))
         {
            _sprite.CurrentAnimation = AnimationKey.Down;
            motion.Y = 1;
         }

         if (InputHandler.KeyDown(Keys.A) ||
            InputHandler.ButtonDown(Buttons.LeftThumbstickLeft, PlayerIndex.One))
         {
            _sprite.CurrentAnimation = AnimationKey.Left;
            motion.X = -1;
         }
         else if (InputHandler.KeyDown(Keys.D) ||
            InputHandler.ButtonDown(Buttons.LeftThumbstickRight, PlayerIndex.One))
         {
            _sprite.CurrentAnimation = AnimationKey.Right;
            motion.X = 1;
         }

         //added controls for animations on diagonals:
         if (InputHandler.KeyDown(Keys.W) && InputHandler.KeyDown(Keys.D) ||
            InputHandler.ButtonDown(Buttons.LeftThumbstickUp, PlayerIndex.One) && InputHandler.ButtonDown(Buttons.LeftThumbstickRight, PlayerIndex.One))
         {
            _sprite.CurrentAnimation = AnimationKey.RU;
         }
         else if (InputHandler.KeyDown(Keys.W) && InputHandler.KeyDown(Keys.A) ||
            InputHandler.ButtonDown(Buttons.LeftThumbstickUp, PlayerIndex.One) && InputHandler.ButtonDown(Buttons.LeftThumbstickLeft, PlayerIndex.One))
         {
            _sprite.CurrentAnimation = AnimationKey.LU;
         }

         if (InputHandler.KeyDown(Keys.S) && InputHandler.KeyDown(Keys.D) ||
            InputHandler.ButtonDown(Buttons.LeftThumbstickDown, PlayerIndex.One) && InputHandler.ButtonDown(Buttons.LeftThumbstickRight, PlayerIndex.One))
         {
            _sprite.CurrentAnimation = AnimationKey.RD;
         }
         else if (InputHandler.KeyDown(Keys.S) && InputHandler.KeyDown(Keys.A) ||
            InputHandler.ButtonDown(Buttons.LeftThumbstickDown, PlayerIndex.One) && InputHandler.ButtonDown(Buttons.LeftThumbstickLeft, PlayerIndex.One))
         {
            _sprite.CurrentAnimation = AnimationKey.LD;
         }

         if (motion != Vector2.Zero)
         {
            _sprite.IsAnimating = true;
            motion.Normalize();

            _sprite.Position += motion * _sprite.Speed;
            _sprite.LockToMap();

            if (_playerView.Camera.CameraMode == CameraMode.Follow)
               _playerView.Camera.LockToSprite(_sprite);
         }
         else
            _sprite.IsAnimating = false;

         if (InputHandler.KeyReleased(Keys.F) ||
            InputHandler.ButtonReleased(Buttons.RightStick, PlayerIndex.One))
         {
            _playerView.Camera.ToggleCameraMode();

            if (_playerView.Camera.CameraMode == CameraMode.Follow)
               _playerView.Camera.LockToSprite(_sprite);
         }

         if (_playerView.Camera.CameraMode != CameraMode.Follow)
         {
            if (InputHandler.KeyReleased(Keys.C) ||
               InputHandler.ButtonReleased(Buttons.LeftStick, PlayerIndex.One))
            {
               _playerView.Camera.LockToSprite(_sprite);
            }
         }

         base.Update(gameTime);
      }
      
      public override void Draw(GameTime gameTime)
      {
         GameRef.SpriteBatch.Begin(
            SpriteSortMode.Deferred,
            BlendState.AlphaBlend,
            SamplerState.PointClamp,
            null,
            null,
            null,
            _playerView.Camera.Transformation
         );

         _map.Draw(GameRef.SpriteBatch, _playerView.Camera);
         _sprite.Draw(gameTime, GameRef.SpriteBatch, _playerView.Camera);

         _sprite.Draw(gameTime, GameRef.SpriteBatch, _playerView.Camera, SecondPlayerPosition.Position);

         SelectionRectangle.Draw(GameRef.SpriteBatch, _selectRectangle);    //draw frame selection

         if(_selected)
            SelectionRectangle.Draw(GameRef.SpriteBatch, _sprite.UnitRectangle);     //draw unit selection

         GameRef.SpriteBatch.End();

         base.Draw(gameTime);
      }

      #endregion

      #region Methods

      private static void NewDataRecived(object sender, byte[] data)
      {
         var playerPosition = PlayerPosition.FromByteArray(data);

         SecondPlayerPosition.Position = playerPosition.Position;
      }

      private void SendDataToServer()
      {
         var playerPosition = new PlayerPosition {Position = _sprite.Position};
         var data = PlayerPosition.ToByteArray(playerPosition);

         _client.SendData(data);
      }

      private byte[] ToByteArray(object source)
      {
         var formatter = new BinaryFormatter();

         using (var stream = new MemoryStream())
         {
            formatter.Serialize(stream, source);

            return stream.ToArray();
         }
      }

      public object ByteArrayToObject(byte[] byteArray)
      {
         try
         {
            // convert byte array to memory stream
            var memoryStream = new MemoryStream(byteArray) { Position = 0 };            // set memory stream position to starting point

            // create new BinaryFormatter
            var binaryFormatter = new BinaryFormatter();

            // Deserializes a stream into an object graph and return as a object.
            return binaryFormatter.Deserialize(memoryStream);
         }
         catch (Exception exception)
         {
            // Error
            Console.WriteLine("Exception caught in process: {0}", exception.ToString());
         }
         // Error occured, return null
         return null;
      }

      #endregion

   }
}