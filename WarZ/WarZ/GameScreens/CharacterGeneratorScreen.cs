﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XRtsLibrary;
using XRtsLibrary.Controls;

namespace WarZ.GameScreens
{
   public class CharacterGeneratorScreen : BaseGameState
   {

      #region Fields

      LeftRightSelector genderSelector;
      LeftRightSelector classSelector;
      PictureBox backgroundImage;

      string[] genderItems = { "Male", "Female" };
      string[] classItems = { "Fighter", "Wizard", "Rogue", "Priest" };

      #endregion

      #region Properties

      #endregion

      #region Constructors

      public CharacterGeneratorScreen(Game game, GameStateManager stateManager)
         : base(game, stateManager)
      {
      }

      #endregion

      #region XNA Methods

      public override void Initialize()
      {
         base.Initialize();
      }

      protected override void LoadContent()
      {
         base.LoadContent();

         CreateControls();
      }

      public override void Update(GameTime gameTime)
      {
         ControlManager.Update(gameTime, PlayerIndex.One);

         base.Update(gameTime);
      }

      public override void Draw(GameTime gameTime)
      {
         GameRef.SpriteBatch.Begin();

         base.Draw(gameTime);

         ControlManager.Draw(GameRef.SpriteBatch);
         
         GameRef.SpriteBatch.End();
      }

      #endregion

      #region Methods

      private void CreateControls()
      {
         Texture2D leftTexture = Game.Content.Load<Texture2D>(@"GUI\leftarrowUp");
         Texture2D rightTexture = Game.Content.Load<Texture2D>(@"GUI\rightarrowUp");
         Texture2D stopTexture = Game.Content.Load<Texture2D>(@"GUI\StopBar");

         backgroundImage = new PictureBox(
            Game.Content.Load<Texture2D>(@"Backgrounds\startscreen2"),
            GameRef.ScreenRectangle
         );

         ControlManager.Add(backgroundImage);

         Label label1 = new Label();

         label1.Text = "Who will search for the Eyes of the Dragon?";
         label1.Size = label1.SpriteFont.MeasureString(label1.Text);
         label1.Position = new Vector2((GameRef.Window.ClientBounds.Width - label1.Size.X) / 2, 450);

         ControlManager.Add(label1);

         genderSelector = new LeftRightSelector(leftTexture, rightTexture, stopTexture);
         //genderSelector.SetItems(genderItems, 125);    modified to below:
         genderSelector.SetItems(genderItems);
         genderSelector.Position = new Vector2(label1.Position.X, 500);

         ControlManager.Add(genderSelector);

         classSelector = new LeftRightSelector(leftTexture, rightTexture, stopTexture);
         //classSelector.SetItems(classItems, 125);       modified to below:
         classSelector.SetItems(classItems);
         classSelector.Position = new Vector2(label1.Position.X, 550);

         ControlManager.Add(classSelector);

         LinkLabel linkLabel1 = new LinkLabel();
         linkLabel1.Text = "Accept this character.";
         linkLabel1.Position = new Vector2(label1.Position.X, 600);
         linkLabel1.Selected += new EventHandler(linkLabel1_Selected);

         ControlManager.Add(linkLabel1);

         ControlManager.NextControl();
      }

      void linkLabel1_Selected(object sender, EventArgs e)
      {
         InputHandler.Flush();

         StateManager.PopState();
         StateManager.PushState(GameRef.GamePlayScreen);
      }

      #endregion

   }
}