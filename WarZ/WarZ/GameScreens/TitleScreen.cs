﻿/* Original author: Jamie McMahon, "SixOfEleven"
 * Source website: http://gameprogrammingadventures.org/eyes-of-the-dragon-tutorial-series/
 * Based on licence: https://creativecommons.org/licenses/by/3.0/ (CC-BY 3.0)
 * Original title: "Eyes of the Dragon"
 */

using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using XRtsLibrary;
using XRtsLibrary.Controls;

namespace WarZ.GameScreens
{
    public class TitleScreen : BaseGameState
   {

      #region Fields

      Texture2D backgroundImage;
      LinkLabel startLabel;

      #endregion

      #region Constructors

      public TitleScreen(Game game, GameStateManager manager)
         : base(game, manager)
      {
      }

      #endregion

      #region XNA Methods

      protected override void LoadContent()
      {
         ContentManager Content = GameRef.Content;

         backgroundImage = Content.Load<Texture2D>(@"Backgrounds\startscreen2");

         base.LoadContent();

         startLabel = new LinkLabel{
            Position = new Vector2(350, 600),
            Text = "Press any key to start...",
            Color = Color.White,
            TabStop = true,
            HasFocus = true
            //Selected += new EventHandler(startLabel_Selected)
         };

         startLabel.Size = startLabel.SpriteFont.MeasureString(startLabel.Text);
         startLabel.Selected += new EventHandler(startLabel_Selected);
         ControlManager.Add(startLabel);
         ControlManager.MeasureControls();
      }

      public override void Update(GameTime gameTime)
      {
         ControlManager.Update(gameTime, PlayerIndex.One);

         base.Update(gameTime);
      }

      public override void Draw(GameTime gameTime)
      {
         GameRef.SpriteBatch.Begin();

         base.Draw(gameTime);

         GameRef.SpriteBatch.Draw(
            backgroundImage,
            GameRef.ScreenRectangle,
            Color.White
         );

         ControlManager.Draw(GameRef.SpriteBatch);

         GameRef.SpriteBatch.End();
      }

      #endregion

      #region Title Screen Methods

      private void startLabel_Selected(object sender, EventArgs e)
      {
         StateManager.PushState(GameRef.StartMenuScreen);
      }

      #endregion

   }
}