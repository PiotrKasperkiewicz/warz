/*
 * Copyright (c) 2013 Tomasz Hachaj
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Net.Sockets;
using System.Threading;
using XRtsLibrary.Globals;

namespace WarZ.Multiplayer
{
   public class TCPClient
   {

      #region Events and Delegates
      
      public delegate void NewDataRecived(object sender, byte[] data);
      public event NewDataRecived NewData;

      #endregion

      #region Fields

      private bool _stopClient;
      private readonly string _addressIP = GameParameters.AddressIP;
      private readonly int _port = GameParameters.Port;
      private TcpClient _clientSocket;

      #endregion

      #region Constructors
      
      public TCPClient(string addressIP, int port)
      {
         _addressIP = addressIP;
         _port = port;
      }

      #endregion

      #region Methods

      public void StartConnection()
      {
         _clientSocket = new TcpClient();
         _clientSocket.Connect(_addressIP, _port);

         var thread = new Thread(ReceiveData);

         thread.Start();
      }

      public void StopConnection()
      {
         _stopClient = true;
         _clientSocket.Close();
      }

      private void ReceiveData()
      {
         try
         {
            while (!_stopClient)
            {
               while (_clientSocket.Available > 0)
               {
                  var serverStream = _clientSocket.GetStream();
                  var inStream = new byte[8];                         //changed size from 85 : sended only Vector2 position (float X, float Y) = 2 * 4 bytes (size of float) = 8

                  serverStream.Read(inStream, 0, 8);                  //as same as above
                  NewData(this, inStream);
               }
               Thread.Sleep(1);
            }
         }
         catch
         {
            Console.WriteLine("No data hasn't been received!");
         }
      }

      public void SendData(byte[] data)
      {
         var serverStream = _clientSocket.GetStream();

         serverStream.Write(data, 0, data.Length);
         serverStream.Flush();
      }

      #endregion

   }
}